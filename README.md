# Environment-Aware Normal Distributions Transform (EA-NDT)

This is the official implementation of EA-NDT, a High-Definiton (HD) map representation that is builds on Normal Distributions Transform (NDT). EA-NDT leverages semantic-aided clustering of a point cloud to compute a compressed map representation that can be used for robot localization. The EA-NDT code is open-source (BSD License) and the code has been developed and tested in Ubuntu 20.04 with ROS Noetic Ninjemys. Please, remember that the code is used in an on-going research and is subject to changes in the future.

<div align="center">
  <img src="ea_ndt.png"  width="100%">
</div>

## Related publications

P. Manninen, H. Hyyti, V. Kyrki, J. Maanpää, J. Taher, J. Hyyppä. **Towards High-Definition Maps: a Framework Leveraging Semantic Segmentation to Improve NDT Map Compression and Descriptivity.** *2022 IEEE/RJS International Conference on Intelligent Robots and Systems (IROS 2022), 2022.* [pdf](https://arxiv.org/abs/2301.03956) <!-- [video]() -->

P. Manninen, P. Litkey, E. Ahokas, J.Maanpää, J. Taher, H. Hyyti, J. Hyyppä. **FGI ARVO VLS-128 LiDAR Point Cloud, Käpylä, 7th of September 2020.** *Finnish Geospatial Research Institute (FGI), 2022.* [dataset](https://zenodo.org/record/6796874)

## System requirements

* Ubuntu 20.04
* ROS Noetic Ninjemys
* Even 64 GB of RAM required with the smallest cell sizes
  * if you are running out of memory, try increasing the cell size.

## Get dataset and extract the data

First, download the dataset from Zenodo: https://zenodo.org/record/6796874

Then, in Linux you can either open the file in File Manager or from terminal install 7zip and extract the downloaded dataset followingly:
```
$ sudo apt-get update
$ sudo apt-get install p7zip-full
$ 7z e cloud.7zip
```

## Set up and build

If you already have ROS Noetic Ninjemys installed, you can skip this:
```
$ sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
$ sudo apt install curl
$ curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
$ sudo apt update
$ sudo apt install ros-noetic-desktop-full
```
if you encounter any problems in the installation of ROS, please check up-to-date instructions from http://wiki.ros.org/noetic/Installation/Ubuntu

Remember to source ROS Noetic:
```
$ source /opt/ros/noetic/setup.bash
```

Install GIT version control:
```
$ sudo apt install git
```

Create a catkin workspace `map_ws`, clone the repository and compile the code:
```
$ mkdir -p ~/map_ws/src
$ cd ~/map_ws/src && git clone https://gitlab.com/fgi_nls/public/hd-map
$ cd .. && catkin_make
```

## Running EA-NDT example

Make sure to source the catkin workspace: 
```
$ source devel/setup.bash
```
Run the launch file in the same path with the extracted dataset:
```
$ roslaunch computeEaNdt.launch
```

##### Computing Normal Distributions Transform (NDT) map representation:
1. The point cloud is divided by a 3D grid
2. Points in each cell are represented by their mean and covariance
3. Scan registration maximizes the fit to the normal distributions

| ![0](viz/ndt.gif)*NDT Map Representation* |
| ------------------------------------- |

##### Computing Environment-Aware (EA)-NDT map representation:
1. Semantic segmentation
2. Instance clustering
3. Primitive extraction (Planar / Cylindrical)
4. Cell clustering
5. Points in each cell represented by their mean and covariance similarly to original NDT

| ![1](viz/eandt.gif)*EA-NDT Map Representation*             | 
| ------------------------------ |

## Visualization

For ROS visualization open a new terminal and type:
```
$ rviz
```
Then in RViz, open the config file [ros_visualization.rviz](https://gitlab.com/fgi_nls/public/hd-map/-/blob/master/ros_visualization.rviz) that is located in the root of the repository. The configuration has following options:
* MarkerArray msgs:
  * EA-NDT Cells
  * NDT Cells
* Marker msg:
  * NDT Grid
* PointCloud2 msgs: 
  * Intensity Point Cloud
  * Semantic Segments
  * Instance Clusters
  * Extracted Primitives
  * Cell Clusters

The point clouds published in ROS are sparse versions but it is possible to save the dense versions as PCD files by turning on the visualize options in `computeEaNdt.launch`. 

## Citing

Thank you for citing if you use EA-NDT codes or FGI ARVO dataset in your work.

##### Publication
```
@article{manninen2022eandt,
  title={Towards High-Definition Maps: a Framework Leveraging Semantic Segmentation to Improve NDT Map Compression and Descriptivity},
  author={Manninen, Petri and Hyyti, Heikki and Kyrki, Ville and Maanp{\"a}{\"a}, Jyri and Taher, Josef and Hyypp{\"a}, Juha},
  journal={2022 IEEE/RJS International Conference on Intelligent Robots and Systems (IROS 2022)},
  pages={5370--5377},
  year={2022},
  publisher={IEEE}
}
```

##### Dataset
```
@misc{manninen2022fgiarvo,
  title={FGI ARVO VLS-128 LiDAR Point Cloud, K{\"a}pyl{\"a}, 7th of September 2020},
  author={Manninen, Petri and Litkey, Paula and Ahokas, Eero and Maanp{\"a}{\"a}, Jyri and Taher, Josef and Hyyti, Heikki and Hyypp{\"a}, Juha},
  year={2022},
  doi={10.5281/zenodo.6796874},
  howpublished{\url{https://zenodo.org/record/6796874}}
}
```

