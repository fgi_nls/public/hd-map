/*
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#include <pcl/io/pcd_io.h>

#include <ros/ros.h>

#include "tools/InstanceClustering.hpp"
#include "MapClustering.hpp"
#include "defs/typedefs.h"

int defineStage(std::vector<std::string> &filepaths, bool force_begin = "false") {

  std::vector<std::vector<std::string>> filepaths_ordered (3);
  for (auto &filepath : filepaths) {
    std::string base_filename = filepath.substr(filepath.find_last_of("/\\") + 1);
    if (base_filename.find("segments") != std::string::npos)
      filepaths_ordered[0].push_back(filepath);
    else if (base_filename.find("instances") != std::string::npos)
      filepaths_ordered[1].push_back(filepath);
    else if (base_filename.find("primitives") != std::string::npos)
      filepaths_ordered[2].push_back(filepath);
  }

  if (force_begin) {
    filepaths = filepaths_ordered[0];
    return 0;
  }
  else if (filepaths_ordered[2].size() > 0) {
    filepaths = filepaths_ordered[2];
    return 2;
  }
  else if (filepaths_ordered[1].size() > 0) {
    filepaths = filepaths_ordered[1];
    return 1;
  }
  else if (filepaths_ordered[0].size() > 0) {
    filepaths = filepaths_ordered[0];
    return 0;
  }

  return -1;
}

void process(ros::NodeHandle node, ros::NodeHandle private_nh) {

  // Get params
  std::string load_path, visualize_path, result_path;
  std::vector<float> cell_size_list;
  bool save_intermediate,
       force_begin,
       visualize_segments,
       visualize_instances,
       visualize_primitives,
       visualize_cells,
       ros_visualization,
       compute_results,
       ndt_comparison,
       use_cell_sizes;

  private_nh.getParam("working_directory", load_path);
  private_nh.getParam("save_intermediate", save_intermediate);
  private_nh.getParam("force_begin", force_begin);
  private_nh.getParam("visualize_segments", visualize_segments);
  private_nh.getParam("visualize_instances", visualize_instances);
  private_nh.getParam("visualize_primitives", visualize_primitives);
  private_nh.getParam("visualize_cells", visualize_cells);
  private_nh.getParam("ros_visualization", ros_visualization);
  private_nh.getParam("compute_results", compute_results);
  private_nh.getParam("ndt_comparison", ndt_comparison);
  private_nh.getParam("cell_size_list", cell_size_list);

  visualize_path = load_path + "visualization/";
  result_path = load_path + "results/";

  if (visualize_segments || visualize_instances || visualize_primitives || visualize_cells) {
    if (!Utils::mkdir(visualize_path)) {
      std::cerr << "Visualization not available, could not create path " << visualize_path << "\n";
      visualize_segments = visualize_instances = visualize_primitives = visualize_cells = false;
    }
  }

  std::cout << "Following parameters are used:" << std::boolalpha << std::endl;
  std::cout << "    Files are loaded from: " << load_path << std::endl;
  std::cout << "    Save intermediate results: " << save_intermediate << std::endl;
  std::cout << "    Force processing from beginning: " << force_begin << std::endl;
  std::cout << "    Save visualization of instances in PCD file: " << visualize_instances << std::endl;
  std::cout << "    Save visualization of primitives in PCD file: " << visualize_primitives << std::endl;
  std::cout << "    Save visualization of cells in PCD file: " << visualize_cells << std::endl;
  std::cout << "    Enable ROS visualization: " << ros_visualization << std::endl;
  std::cout << "    Compute and print results: " << compute_results << std::endl;
  std::cout << "    Compare to NDT: " << ndt_comparison << std::endl;
  std::cout << "    Cell sizes: [";
  for (auto& size : cell_size_list)
    std::cout << size << ", ";
  std::cout << "]\n\n";


  // Load point clouds
  std::vector<std::string> filepaths;
  try {
    Utils::glob( load_path + "cloud*.pcd", filepaths );
  }
  catch (std::runtime_error &e) {
    std::cerr << "Are you sure that the PCD files with the correct naming convention are located in path " << load_path << "?" << std::endl;
    return;
  }

  // Declare object to create the map
  pcl::console::TicToc tt;
  MapClustering<PointTD2L> mapClustering;
  if (save_intermediate)
    mapClustering.setSaveIntermediateResults(load_path);

  switch (defineStage(filepaths, force_begin)) {
    case 0: { // Starting to process from semantically segmented point clouds
      std::cout << "Starting to process from the semantic segmented point clouds." << std::endl;

      CloudTD2LPtr cloud (new CloudTD2L);
      {
        CloudTDLabelProbPtr loadCloud (new CloudTDLabelProb);
        for (auto &filepath : filepaths) {
          CloudTDLabelProbPtr subCloud (new CloudTDLabelProb);
          tt.tic ();
          pcl::io::loadPCDFile<PointTDLabelProb> (filepath, *subCloud);
          std::string base_filename = filepath.substr(filepath.find_last_of("/\\") + 1);
          std::cout << base_filename << " opened in " << tt.toc () / 1e3 << " secs.\n";
          *loadCloud += *subCloud;

          if (visualize_segments) {
            tt.tic ();
            CloudRGB subCloudRGB;
            makeRGBIntensityCloud(*subCloud, subCloudRGB);
            base_filename = "RGB_" + base_filename;
            std::string save_path = visualize_path + base_filename;
            pcl::io::savePCDFile<PointRGB> (save_path, subCloudRGB);
            std::cout << "Visualization of semantic segments " << base_filename << " saved in " << tt.toc () / 1e3 << " secs.\n";
          }
        }
        pcl::copyPointCloud(*loadCloud, *cloud);
      }

      mapClustering.clusterInstances(cloud);

      if (visualize_instances)
        mapClustering.saveVisualizedInstances(visualize_path);

      break;
    }
    case 1: { // Instance clustered point clouds found, will continue the process from there
      std::cout << "Starting to process from the instance clustered point clouds." << std::endl;

      std::cout << "\nInitializing primitives from cluster instance clouds:\n";
      for (auto &filepath : filepaths) {

        tt.tic ();
        std::string base_filename = filepath.substr(filepath.find_last_of("/\\") + 1);
        CloudTD2LPtr cloudInstances (new CloudTD2L);
        pcl::io::loadPCDFile<PointTD2L> (filepath, *cloudInstances);
        std::cout << base_filename << " opened in " << tt.toc () / 1e3 << " secs.\n";

        if (base_filename.find("building") != std::string::npos)
          mapClustering.setInputCloudBuildingInstances(cloudInstances);
        else if (base_filename.find("fence") != std::string::npos)
          mapClustering.setInputCloudFenceInstances(cloudInstances);
        else if (base_filename.find("ground") != std::string::npos)
          mapClustering.setInputCloudGroundInstances(cloudInstances);
        else if (base_filename.find("pole") != std::string::npos)
          mapClustering.setInputCloudPoleInstances(cloudInstances);
        else if (base_filename.find("sign") != std::string::npos)
          mapClustering.setInputCloudSignInstances(cloudInstances);
        else if (base_filename.find("trunk") != std::string::npos)
          mapClustering.setInputCloudTrunkInstances(cloudInstances);
      }

      break;
    }
    case 2: { // Primitive extracted point clouds found, will continue the process from there
      std::cout << "Starting to process from the primitive extracted point clouds." << std::endl;

      for (auto &filepath : filepaths) {

        tt.tic ();
        std::string base_filename = filepath.substr(filepath.find_last_of("/\\") + 1);
        CloudTD2LPtr cloudPrimitives (new CloudTD2L);
        pcl::io::loadPCDFile<PointTD2L> (filepath, *cloudPrimitives);
        std::cout << base_filename << " opened in " << tt.toc () / 1e3 << " secs.\n";

        if (base_filename.find("building") != std::string::npos)
          mapClustering.setInputCloudBuildingPrimitives(cloudPrimitives);
        else if (base_filename.find("fence") != std::string::npos)
          mapClustering.setInputCloudFencePrimitives(cloudPrimitives);
        else if (base_filename.find("ground") != std::string::npos)
          mapClustering.setInputCloudGroundPrimitives(cloudPrimitives);
        else if (base_filename.find("pole") != std::string::npos)
          mapClustering.setInputCloudPolePrimitives(cloudPrimitives);
        else if (base_filename.find("sign") != std::string::npos)
          mapClustering.setInputCloudSignPrimitives(cloudPrimitives);
        else if (base_filename.find("trunk") != std::string::npos)
          mapClustering.setInputCloudTrunkPrimitives(cloudPrimitives);
      }
      break;
    }
    default:
      std::cout << "Something went wrong, appropriate pcd files were not found in the given path." << std::endl;
      break;
  }

  if (visualize_primitives)
    mapClustering.saveVisualizedPrimitives(visualize_path);

  // Start processing from the larger cell sizes because they are faster to process
  std::reverse(cell_size_list.begin(), cell_size_list.end());
  for (auto &cell_size : cell_size_list) {
    mapClustering.setCellSize (cell_size);
    mapClustering.process ();

    if (ndt_comparison)
      mapClustering.computeNdtRepresentation();

    if (ros_visualization)
      mapClustering.rosPublisher(node, true, true, true, true);

    if (visualize_cells)
      mapClustering.saveVisualizedCells(visualize_path);

    if (compute_results) {
      mapClustering.computeEaNdtFitnessScore();
      mapClustering.computeNdtFitnessScore();
    }

    mapClustering.reset ();
  }

  if (compute_results) {
    if (Utils::mkdir(result_path))
      mapClustering.saveScores2CSV(result_path);
    else
      std::cerr << "Results not available, could not create path " << result_path << "\n";
  }
}


int main(int argc, char** argv) {

  ros::init(argc, argv, "instance_representation");
  ros::NodeHandle node;
  ros::NodeHandle priv_nh("~");

  process( node, priv_nh );

  return 0;
}
