/*
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREEMAP_HPP_
#define SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREEMAP_HPP_

#include <string>
#include <fstream>
#include <vector>
#include <limits>

#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/device/back_inserter.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <pcl/octree/octree_base.h>
#include <pcl/octree/octree_pointcloud.h>
#include "octree_covariance_container.h"
#include <utils/utils.h>

template <typename OctreeT, typename PointT>
class OctreeMap{
public:
  typedef typename pcl::PointCloud<PointT>::Ptr CloudPtr;
  typedef typename pcl::PointCloud<PointT>::ConstPtr CloudConstPtr;
  typedef typename pcl::octree::OctreeCovarianceContainer<PointT> LeafContainer;

  OctreeMap(const OctreeT &octree, const double &resolution = 0);
  OctreeMap(const double &resolution = 0.1f);
  ~OctreeMap();

  void loadMap(const std::string &path);
  void saveMap(const std::string &path);

  double computeFitnessScore(CloudConstPtr cloud, std::vector<float> &scores) const;
  void getCentroidCloud(CloudPtr cloud) const;

private:
  void Init(const double &resolution);

  OctreeT octree_;
  double outlier_ratio_, resolution_;
  double gauss_d1_, gauss_d2_, gauss_d3_;

  std::vector<char> binary_tree_;
};


template <typename OctreeT, typename PointT>
OctreeMap<OctreeT, PointT>::OctreeMap(const double &resolution)
: octree_(resolution) {
  Init(resolution);
}


template <typename OctreeT, typename PointT>
OctreeMap<OctreeT, PointT>::OctreeMap(const OctreeT &octree, const double &resolution) {
  octree_ = octree;
  Init(resolution);
}


template <typename OctreeT, typename PointT>
OctreeMap<OctreeT, PointT>::~OctreeMap() { }


template <typename OctreeT, typename PointT>
void
OctreeMap<OctreeT, PointT>::Init(const double &resolution) {
  if (!resolution)
    resolution_ = octree_->getResolution();
  else
    resolution_ = resolution;

  outlier_ratio_ = 0.55;
  double gauss_c1 = 10 * (1 - outlier_ratio_);
  double gauss_c2 = outlier_ratio_ / pow (resolution_, 3);
  gauss_d3_ = -log (gauss_c2);
  gauss_d1_ = -log ( gauss_c1 + gauss_c2 ) - gauss_d3_;
  gauss_d2_ = -2 * log ((-log ( gauss_c1 * exp ( -0.5 ) + gauss_c2 ) - gauss_d3_) / gauss_d1_);
}

template <typename OctreeT, typename PointT>
void
OctreeMap<OctreeT, PointT>::loadMap(const std::string &path) {
  std::ifstream file(path, std::ios::binary);
  std::vector<char> binary_tree((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

  for (int i = 0; i < binary_tree_.size(); i++) {
    if (binary_tree[i] != binary_tree_[i])
      std::cout << i << " " << binary_tree[i] << " " << binary_tree_[i] << std::endl;
  }

  ROS_INFO_STREAM("Load octree map vector size: " << binary_tree.size());

  octree_->deserializeTree(binary_tree);
}


template <typename OctreeT, typename PointT>
void
OctreeMap<OctreeT, PointT>::saveMap(const std::string &path) {
  std::vector<LeafContainer*> leafs;
  octree_->serializeTree(binary_tree_, leafs);

  ROS_INFO_STREAM("Save octree map vector size: " << binary_tree_.size() << ", leafs: " << leafs.size());

  std::ofstream file_tree(path + "octree_map_tree.bin", std::ios::binary);
  for (const auto &element : binary_tree_)
    file_tree << element;
}


template <typename OctreeT, typename PointT>
double
OctreeMap<OctreeT, PointT>::computeFitnessScore(CloudConstPtr cloud, std::vector<float> &scores) const {

  scores.resize(cloud->points.size());
  std::vector<double> score_sum (omp_get_max_threads());

  #pragma omp parallel for schedule(guided, 128)
  for (int idx = 0; idx < cloud->points.size(); idx++) {
    PointT point = cloud->points[idx];

    // SEARCH METHOD
    std::vector<float> k_sqr_distances;
    auto neighborhood = octree_->radiusSearch(point, 2*resolution_, k_sqr_distances);

    double score = -1;
    for (auto &leafContainer : neighborhood)
    {
      if (leafContainer.getPointCount() < 6) {
        if (score == -1)
          score = -2;
        continue;
      }

      Eigen::Vector3d pointVec = Eigen::Vector3d(point.x, point.y, point.z);

      // Denorm point, x_k' in Equations 6.12 and 6.13 [Magnusson 2009]
      pointVec -= leafContainer.getMean();
      // Uses precomputed covariance for speed.
      Eigen::Matrix3d c_inv = leafContainer.getInverseCov();

      // e^(-d_2/2 * (x_k - mu_k)^T Sigma_k^-1 (x_k - mu_k)) Equation 6.9 [Magnusson 2009]
      double e_x_cov_x = exp(-1 * pointVec.dot(c_inv * pointVec) / 2);
      // Calculate probability of transformed points existance, Equation 6.9 [Magnusson 2009]
      double score_inc = leafContainer.getNormalizationTerm() * e_x_cov_x;

      if (score_inc > score)
        score = score_inc;
    }

    scores[idx] = float(score);
    score_sum[omp_get_thread_num()] += score;
  }

  double overall_sum = 0;
  for (auto &thread_sum : score_sum)
    overall_sum += thread_sum;

  return (overall_sum / static_cast<double> (cloud->size()));
}

template <typename OctreeT, typename PointT>
void
OctreeMap<OctreeT, PointT>::getCentroidCloud(CloudPtr cloud) const {
  typename pcl::octree::OctreePointCloud<PointT>::AlignedPointTVector centroids;
  octree_->getVoxelCentroids(centroids);

  cloud->points.assign (centroids.begin(), centroids.end());
  cloud->width = uint32_t (centroids.size());
  cloud->height = 1;
  cloud->is_dense = false;
}



#endif /* SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREEMAP_HPP_ */
