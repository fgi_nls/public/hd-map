/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2010-2011, Willow Garage, Inc.
 *  Copyright (c) 2012-, Open Perception, Inc.
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREE_COVARIANCE_CONTAINER_H_
#define SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREE_COVARIANCE_CONTAINER_H_

#pragma once

#include <boost/serialization/string.hpp>
#include <boost/serialization/serialization.hpp>

#include <Eigen/Eigenvalues>

#include <pcl/octree/octree_base.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/impl/voxel_grid.hpp>

template class pcl::VoxelGrid<PointTD2L>;

namespace pcl
{
  namespace octree
  {
    /** \brief @b Octree pointcloud voxel centroid leaf node class
      * \note This class implements a leaf node that calculates the mean centroid of all points added this octree container.
      * \author Julius Kammerl (julius@kammerl.de)
      */
    template<typename PointT>
    class OctreeCovarianceContainer : public OctreeContainerBase
    {
      public:
        typedef typename pcl::PointCloud<PointT> Cloud;
        typedef typename pcl::PointCloud<PointT>::Ptr CloudPtr;
        typedef typename pcl::PointCloud<PointT>::ConstPtr CloudConstPtr;

        /** \brief Class initialization. */
        OctreeCovarianceContainer () :
          leafDataTVector_ (new pcl::Indices)
        {
          this->reset();
        }

        /** \brief Empty class deconstructor. */
        ~OctreeCovarianceContainer ()
        {
        }

        /** \brief deep copy function */
        virtual OctreeCovarianceContainer *
        deepCopy () const
        {
          return (new OctreeCovarianceContainer (*this));
        }

        /** \brief Equal comparison operator - set to false
         */
         // param[in] OctreeCovarianceContainer to compare with
        bool operator==(const OctreeContainerBase&) const override
        {
          return ( false );
        }

        /** \brief Add new point to voxel.
          * \param[in] new_point the new point to add
          */
        void
        addPoint (const PointT& new_point, const std::size_t &index)
        {
          using namespace pcl::common;

          leafDataTVector_->push_back(index);
          ++point_counter_;

          point_sum_ += new_point;
          Eigen::Vector3d pt3d (new_point.x, new_point.y, new_point.z);
          pt_sum_ += pt3d;
          cov_sum_ += pt3d * pt3d.transpose ();
        }

        /** \brief Calculate centroid of voxel.
          * \param[out] centroid_arg the resultant centroid of the voxel
          */
        void
        getCentroid (PointT& centroid_arg) const
        {
          using namespace pcl::common;

          if (point_counter_)
          {
            centroid_arg = point_sum_;
            centroid_arg /= static_cast<float> (point_counter_);
          }
          else
          {
            centroid_arg *= 0.0f;
          }
        }

        bool
        initContainerValues(CloudConstPtr cloud, double min_cov_eigenvalue_multiplier = 0.01) {

          // Normalize mean
          if (point_counter_ < 1) {
            //std::cerr << "Cannot compute statistics, container point_counter = " << point_counter_ << std::endl;
            return false;
          }

          mean_ = pt_sum_ / point_counter_;

          cov_ = (cov_sum_ - 2 * (pt_sum_ * mean_.transpose())) / point_counter_ + mean_ * mean_.transpose ();
          cov_ = cov_ * ((point_counter_ - 1.0) / point_counter_);

          Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eigensolver;

          //Normalize Eigen Val such that max no more than 100x min.
          eigensolver.compute (cov_);
          Eigen::Matrix3d eigen_val = eigensolver.eigenvalues().asDiagonal();
          evecs_ = eigensolver.eigenvectors();

          if (eigen_val (0, 0) < 0 || eigen_val (1, 1) < 0 || eigen_val (2, 2) <= 0) {
            point_counter_ = -1;
            return false;
          }

          // Avoids matrices near singularities (eq 6.11)[Magnusson 2009]
          double min_covar_eigvalue = min_cov_eigenvalue_multiplier * eigen_val (2, 2);
          if (eigen_val (0, 0) < min_covar_eigvalue) {
            eigen_val (0, 0) = min_covar_eigvalue;
            if (eigen_val (1, 1) < min_covar_eigvalue)
              eigen_val (1, 1) = min_covar_eigvalue;

            cov_ = evecs_ * eigen_val * evecs_.inverse ();
          }
          evals_ = eigen_val.diagonal ();

          Eigen::Matrix3d pointCov;
          pointCov << 0.05, 0,       0,
                      0   , 0.05,    0,
                      0   , 0   , 0.05;
          cov_ += pointCov;
          normalizationTerm_ = 1 / sqrt(pow(2.f * M_PI, 3) * cov_.determinant());

          icov_ = cov_.inverse ();
          if (icov_.maxCoeff () ==  std::numeric_limits<float>::infinity ( ) ||
              icov_.minCoeff () == -std::numeric_limits<float>::infinity ( ) ) {
            point_counter_ = -1;
          }

          return true;
        }

        /** \brief Get the voxel centroid.
          * \return centroid
          */
        Eigen::Vector3d
        getMean () const {
          return (mean_);
        }

        /** \brief Get the voxel covariance.
          * \return covariance matrix
          */
        Eigen::Matrix3d
        getCov () const {
          return (cov_);
        }

        /** \brief Get the inverse of the voxel covariance.
          * \return inverse covariance matrix
          */
        Eigen::Matrix3d
        getInverseCov () const {
          return (icov_);
        }

        /** \brief Get the eigen vectors of the voxel covariance.
          * \note Order corresponds with \ref getEvals
          * \return matrix whose columns contain eigen vectors
          */
        Eigen::Matrix3d
        getEvecs () const {
          return (evecs_);
        }

        /** \brief Get the eigen values of the voxel covariance.
          * \note Order corresponds with \ref getEvecs
          * \return vector of eigen values
          */
        Eigen::Vector3d
        getEvals () const {
          return (evals_);
        }

        /** \brief Get the number of points contained by this voxel.
          * \return number of points
          */
        int
        getPointCount () const {
          return (point_counter_);
        }

        double
        getNormalizationTerm() const {
          return (normalizationTerm_);
        }

        /** \brief Reset leaf container. */
        void
        reset () override
        {
          using namespace pcl::common;

          point_counter_ = 0;
          point_sum_ *= 0.0f;
          cov_sum_ = Eigen::Matrix3d::Zero();
          mean_ = Eigen::Vector3d::Zero();
          pt_sum_ = Eigen::Vector3d::Zero();
          cov_ = Eigen::Matrix3d::Zero();
          icov_ = Eigen::Matrix3d::Zero();
          evecs_ = Eigen::Matrix3d::Zero();
          evals_ = Eigen::Vector3d::Zero();
          leafDataTVector_->clear();
          pointVolume_ = 0.f;
          normalizationTerm_ = 0.f;
        }

      private:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & point_counter_;
        }

        int point_counter_;
        PointT point_sum_;
        Eigen::Matrix3d cov_sum_;

        /** \brief 3D voxel centroid */
        Eigen::Vector3d pt_sum_;

        /** \brief 3D voxel centroid */
        Eigen::Vector3d mean_;

        /** \brief Voxel covariance matrix */
        Eigen::Matrix3d cov_;

        /** \brief Inverse of voxel covariance matrix */
        Eigen::Matrix3d icov_;

        /** \brief Eigen vectors of voxel covariance matrix */
        Eigen::Matrix3d evecs_;

        /** \brief Eigen values of voxel covariance matrix */
        Eigen::Vector3d evals_;

        /** \brief Indices Vector */
        pcl::IndicesPtr leafDataTVector_;

        /** \brief Volume estimate for all points */
        float pointVolume_;

        /** \brief Covariance matrix determinant */
        double normalizationTerm_;
    };
  }
}

#endif /* SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREE_COVARIANCE_CONTAINER_H_ */
