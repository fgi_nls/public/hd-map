/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2010-2011, Willow Garage, Inc.
 *  Copyright (c) 2012-, Open Perception, Inc.
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREE_GRID_COVARIANCE_H_
#define SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREE_GRID_COVARIANCE_H_

#pragma once

//#include <pcl/octree/octree_pointcloud.h>
#include <pcl/octree/octree_search.h>
#include "octree_covariance_container.h"

namespace pcl
{
  namespace octree
  {
    /** \brief @b Octree pointcloud voxel centroid class
      * \note This class generate an octrees from a point cloud (zero-copy). It provides a vector of centroids for all occupied voxels.
      * \note The octree pointcloud is initialized with its voxel resolution. Its bounding box is automatically adjusted or can be predefined.
      * \note
      * \note typename: PointT: type of point used in pointcloud
      *
      * \ingroup octree
      * \author Julius Kammerl (julius@kammerl.de)
      */
    template<typename PointT,
             typename LeafContainerT = OctreeCovarianceContainer<PointT> ,
             typename BranchContainerT = OctreeContainerEmpty >
    class OctreeGridCovariance : public OctreePointCloudSearch<PointT, LeafContainerT, BranchContainerT>
    {
      public:
        using Ptr = shared_ptr<OctreeGridCovariance<PointT, LeafContainerT> >;
        using ConstPtr = shared_ptr<const OctreeGridCovariance<PointT, LeafContainerT> >;

        using OctreeT = OctreePointCloud<PointT, LeafContainerT, BranchContainerT>;
        using LeafNode = typename OctreeT::LeafNode;
        using BranchNode = typename OctreeT::BranchNode;

        /** \brief OctreeGridCovariances class constructor.
          * \param[in] resolution_arg octree resolution at lowest octree level
          */
        OctreeGridCovariance (const double resolution_arg) :
          OctreePointCloudSearch<PointT, LeafContainerT, BranchContainerT> (resolution_arg)
        {
        }

        /** \brief Empty class deconstructor. */

        ~OctreeGridCovariance ()
        {
        }

        /** \brief Add DataT object to leaf node at octree key.
          * \param pointIdx_arg
          */
        void
        addPointIdx (const int pointIdx_arg)
        {
          OctreeKey key;

          assert (pointIdx_arg < static_cast<int> (this->input_->points.size ()));

          const PointT& point = this->input_->points[pointIdx_arg];

          // make sure bounding box is big enough
          this->adoptBoundingBoxToPoint (point);

          // generate key
          this->genOctreeKeyforPoint (point, key);

          // add point to octree at key
          LeafContainerT* container = this->createLeaf(key);
          container->addPoint (point, pointIdx_arg);
        }

        /**  \Overriding function to add points from input cloud to octree, points need to be added cluster by cluster
          *  \param[in] point describing the center of the cluster
          */

        void
        addPointsFromInputCloud() {
          if (this->indices_) {
            for (const auto& index : *this->indices_) {
              assert((index >= 0) && (static_cast<std::size_t>(index) < this->input_->size()));

              if (isFinite((*this->input_)[index])) {
                // add points to octree
                this->addPointIdx(index);
              }
            }
          }
          else {
            for (std::size_t i = 0; i < static_cast<std::size_t>(this->input_->size()); i++) {
              if (isFinite((*this->input_)[i])) {
                // add points to octree
                this->addPointIdx(i);
              }
            }
          }
          initContainerValues();
        }

        std::vector<LeafContainerT>
        nearestKSearch(const PointT& p_q, std::size_t k) {
          assert(this->leaf_count_ > 0);
          assert(isFinite(p_q) &&
                 "Invalid (NaN, Inf) point coordinates given to nearestKSearch!");

          std::vector<LeafContainerT> leafs;
          if (k < 1)
            return leafs;

          OctreeKey key;
          key.x = key.y = key.z = 0;

          // initialize smallest point distance in search with high value
          double smallest_dist = std::numeric_limits<double>::max();


          getKNearestNeighborRecursive(p_q, k, this->root_node_, key, 1, smallest_dist, leafs);

          return leafs;
        }

        double
        getKNearestNeighborRecursive(const PointT& point,
                                     std::size_t K,
                                     const BranchNode* node,
                                     const OctreeKey& key,
                                     std::size_t tree_depth,
                                     const double squared_search_radius,
                                     std::vector<LeafContainerT> &leafs) const;


        std::vector<LeafContainerT>
        radiusSearch(const PointT& p_q, const double radius, std::vector<float>& k_sqr_distances, std::size_t max_nn = 0) {
          assert(isFinite(p_q) &&
                 "Invalid (NaN, Inf) point coordinates given to nearestKSearch!");
          OctreeKey key;
          key.x = key.y = key.z = 0;

          std::vector<LeafContainerT> leafs;
          getNeighborsWithinRadiusRecursive(p_q,
                                            radius * radius,
                                            this->root_node_,
                                            key,
                                            1,
                                            leafs,
                                            k_sqr_distances,
                                            max_nn);

          return leafs;
        }

        void
        getNeighborsWithinRadiusRecursive(const PointT& point,
                                          const double radiusSquared,
                                          const BranchNode* node,
                                          const OctreeKey& key,
                                          std::size_t tree_depth,
                                          std::vector<LeafContainerT> &leafs,
                                          std::vector<float>& k_sqr_distances,
                                          std::size_t max_nn) const;

        /** \brief Get centroid for a single voxel addressed by a PointT point.
          * \param[in] point_arg point addressing a voxel in octree
          * \param[out] voxel_centroid_arg centroid is written to this PointT reference
          * \return "true" if voxel is found; "false" otherwise
          */
        bool
        getVoxelCentroidAtPoint (const PointT& point_arg, PointT& voxel_centroid_arg) const;

        /** \brief Get centroid for a single voxel addressed by a PointT point from input cloud.
          * \param[in] point_idx_arg point index from input cloud addressing a voxel in octree
          * \param[out] voxel_centroid_arg centroid is written to this PointT reference
          * \return "true" if voxel is found; "false" otherwise
          */
        inline bool
        getVoxelCentroidAtPoint (const int& point_idx_arg, PointT& voxel_centroid_arg) const
        {
          // get centroid at point
          return (this->getVoxelCentroidAtPoint (this->input_->points[point_idx_arg], voxel_centroid_arg));
        }

        /** \brief Get PointT vector of centroids for all occupied voxels.
          * \param[out] voxel_centroid_list_arg results are written to this vector of PointT elements
          * \return number of occupied voxels
          */
        std::size_t
        getVoxelCentroids (typename OctreePointCloud<PointT, LeafContainerT, BranchContainerT>::AlignedPointTVector
                              &voxel_centroid_list_arg) const {
          OctreeKey new_key;

          // reset output vector
          voxel_centroid_list_arg.clear ();
          voxel_centroid_list_arg.reserve (this->leaf_count_);

          getVoxelCentroidsRecursive (this->root_node_, new_key, voxel_centroid_list_arg );

          // return size of centroid vector
          return (voxel_centroid_list_arg.size ());
        }

        /** \brief Recursively explore the octree and output a PointT vector of centroids for all occupied voxels.
          * \param[in] branch_arg: current branch node
          * \param[in] key_arg: current key
          * \param[out] voxel_centroid_list_arg results are written to this vector of PointT elements
          */
        void
        getVoxelCentroidsRecursive (const BranchNode* branch_arg,
                                    OctreeKey& key_arg,
                                    typename OctreePointCloud<PointT, LeafContainerT, BranchContainerT>::AlignedPointTVector &voxel_centroid_list_arg) const;

        void initContainerValues() const {
          OctreeKey new_key;
          initContainerValuesRecursive (this->root_node_, new_key);

          return;
        }

        void initContainerValuesRecursive(const BranchNode* branch_arg, OctreeKey& key_arg) const;

      protected:
        class prioBranchQueueEntry {
        public:
          /** \brief Empty constructor  */
          prioBranchQueueEntry() : node(), point_distance(0) {}

          /** \brief Constructor for initializing priority queue entry.
           * \param _node pointer to octree node
           * \param _key octree key addressing voxel in octree structure
           * \param[in] _point_distance distance of query point to voxel center
           */
          prioBranchQueueEntry(OctreeNode* _node, OctreeKey& _key, float _point_distance)
          : node(_node), point_distance(_point_distance), key(_key)
          {}

          /** \brief Operator< for comparing priority queue entries with each other.
           * \param[in] rhs the priority queue to compare this against
           */
          bool
          operator<(const prioBranchQueueEntry rhs) const
          {
            return (this->point_distance > rhs.point_distance);
          }

          /** \brief Pointer to octree node. */
          const OctreeNode* node;

          /** \brief Distance to query point. */
          float point_distance;

          /** \brief Octree key. */
          OctreeKey key;
        };
    };
  }
}

// Note: Don't precompile this octree type to speed up compilation. It's probably rarely
// used.
#include "octree_grid_covariance.hpp"

#endif /* SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREE_GRID_COVARIANCE_H_ */
