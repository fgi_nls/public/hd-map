/*
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_KMEANS_HPP_
#define SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_KMEANS_HPP_

#include <pcl/segmentation/conditional_euclidean_clustering.h>
#include <defs/typedefs.h>

#include "dkm_parallel.hpp"


template <typename PointT>
class Kmeans{
public:
  typedef typename pcl::PointCloud<PointT> Cloud;
  typedef typename pcl::PointCloud<PointT>::Ptr CloudPtr;
  typedef typename pcl::PointCloud<PointT>::ConstPtr CloudConstPtr;

  Kmeans();
  ~Kmeans();

  void setInputCloud(CloudPtr cloud, pcl::IndicesPtr indices = nullptr);
  void process2D(int numClusters);
  void process(int numClusters);
  void getClusters(pcl::IndicesClustersPtr clusterIndices);
  void getMeans(CloudPtr means);

private:
  CloudPtr cloud_;
  pcl::IndicesPtr indices_;
  pcl::IndicesClustersPtr clusterIndices_;

  std::tuple<std::vector<std::array<float, 3>>, std::vector<uint32_t>> data_;
  std::tuple<std::vector<std::array<float, 2>>, std::vector<uint32_t>> data2D_;

  bool is2D_;
};


template <typename PointT>
Kmeans<PointT>::Kmeans()
: cloud_ (new Cloud),
  indices_ (new pcl::Indices),
  clusterIndices_ (new pcl::IndicesClusters),
  is2D_ (false) { }


template <typename PointT>
Kmeans<PointT>::~Kmeans() { }


template <typename PointT>
void
Kmeans<PointT>::setInputCloud(CloudPtr cloud, pcl::IndicesPtr indices) {
  cloud_ = cloud;
  indices_ = indices;
}

template <typename PointT>
void
Kmeans<PointT>::process2D(int numClusters) {
  is2D_ = true;
  std::vector<std::array<float, 2>> data;

  if (indices_) {
    data.reserve(indices_->size());
    for (const auto &idx : *indices_) {
      std::array<float, 2> pt;
      pt[0] = cloud_->points[idx].x;
      pt[1] = cloud_->points[idx].y;
      data.push_back(pt);
    }
  }
  else {
    data.reserve(cloud_->size());
    for (const auto &point : cloud_->points) {
      std::array<float, 2> pt;
      pt[0] = point.x;
      pt[1] = point.y;
      data.push_back(pt);
    }
  }

  // K-means code is from https://github.com/genbattle/dkm
  data2D_ = dkm::kmeans_lloyd_parallel(data, numClusters);

  clusterIndices_->resize(std::get<0>(data2D_).size());
  int pointId = 0;

  if (indices_) {
    for (const auto& label : std::get<1>(data2D_)) {
      clusterIndices_->at(label).indices.push_back(indices_->at(pointId));
      pointId++;
    }
  }
  else {
    for (const auto& label : std::get<1>(data2D_)) {
      clusterIndices_->at(label).indices.push_back(pointId);
      pointId++;
    }
  }
}


template <typename PointT>
void
Kmeans<PointT>::process(int numClusters) {
  is2D_ = false;
  std::vector<std::array<float, 3>> data;

  if (indices_) {
    data.reserve(indices_->size());
    for (const auto &idx : *indices_) {
      std::array<float, 3> pt;
      pt[0] = cloud_->points[idx].x;
      pt[1] = cloud_->points[idx].y;
      pt[2] = cloud_->points[idx].z;
      data.push_back(pt);
    }
  }
  else {
    data.reserve(cloud_->size());
    for (const auto &point : cloud_->points) {
      std::array<float, 3> pt;
      pt[0] = point.x;
      pt[1] = point.y;
      pt[2] = point.z;
      data.push_back(pt);
    }
  }

  // K-means code is from https://github.com/genbattle/dkm
  data_ = dkm::kmeans_lloyd_parallel(data, numClusters);

  clusterIndices_->resize(std::get<0>(data_).size());
  int pointId = 0;

  if (indices_) {
    for (const auto& label : std::get<1>(data_)) {
      clusterIndices_->at(label).indices.push_back(indices_->at(pointId));
      pointId++;
    }
  }
  else {
    for (const auto& label : std::get<1>(data_)) {
      clusterIndices_->at(label).indices.push_back(pointId);
      pointId++;
    }
  }
}


template <typename PointT>
void
Kmeans<PointT>::getClusters(pcl::IndicesClustersPtr clusterIndices) {
  *clusterIndices = *clusterIndices_;
}


template <typename PointT>
void
Kmeans<PointT>::getMeans(CloudPtr means) {
  means->reserve(std::get<0>(data_).size());

  if (is2D_) {
    int label = 0;
    for (const auto& mean : std::get<0>(data2D_)) {
      PointT point;
      point.x = mean[0];
      point.y = mean[1];

      //auto pointIndices = clusterIndices_->at(label);
      double z = 0;
      for (auto pointIndex : clusterIndices_->at(label).indices)
        z += cloud_->points[pointIndex].z;

      point.z = z / clusterIndices_->at(label).indices.size();
      label++;
      means->push_back(point);
    }
  }
  else {
    for (const auto& mean : std::get<0>(data_)) {
      PointT point;
      point.x = mean[0];
      point.y = mean[1];
      point.z = mean[2];
      means->push_back(point);
    }
  }
}


#endif /* SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_KMEANS_HPP_ */
