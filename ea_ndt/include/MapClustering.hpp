/*
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_MAPCLUSTERING_HPP_
#define SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_MAPCLUSTERING_HPP_

#include <algorithm>
#include <random>
#include <map>

#include <pcl/io/pcd_io.h>
#include <pcl/impl/instantiate.hpp>
#include <pcl/console/time.h>
#include <pcl/common/common.h>
#include <pcl/common/pca.h>
#include <pcl/search/kdtree.h>
#include <pcl/search/impl/kdtree.hpp>
#include <pcl/search/impl/organized.hpp>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/impl/pcl_base.hpp>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/segmentation/impl/extract_clusters.hpp>
#include <pcl/octree/octree_pointcloud_pointvector.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/impl/extract_indices.hpp>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/impl/voxel_grid.hpp>
#include <pcl/filters/crop_box.h>
#include <pcl/filters/impl/crop_box.hpp>

#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

#include <visualization_msgs/MarkerArray.h>

#include "PlaneSegmentation.hpp"
#include "Kmeans.hpp"
#include "OctreeMap.hpp"
#include "octree_covariance_container.h"
#include "octree_grid_covariance.h"
#include "octree_segment_covariance.h"
#include "tools/InstanceClustering.hpp"
#include "utils/conio.h"

template class pcl::EuclideanClusterExtraction<PointTD2L>;
template class pcl::ExtractIndices<PointTD2L>;
template class pcl::CropBox<PointTD2L>;

template <typename PointT>
class MapClustering {
public:
  typedef typename pcl::PointCloud<PointT> Cloud;
  typedef typename pcl::PointCloud<PointT>::Ptr CloudPtr;
  typedef typename pcl::PointCloud<PointT>::ConstPtr CloudConstPtr;

  typedef typename pcl::octree::OctreeSegmentCovariance<PointT> OctreeSegmentT;
  typedef typename pcl::octree::OctreeSegmentCovariance<PointT>::Ptr OctreeSegmentTPtr;

  typedef typename pcl::octree::OctreeGridCovariance<PointT> OctreeGridT;
  typedef typename pcl::octree::OctreeGridCovariance<PointT>::Ptr OctreeGridTPtr;

  typedef typename pcl::search::KdTree<PointT> KdTreeT;
  typedef typename pcl::search::KdTree<PointT>::Ptr KdTreeTPtr;

  MapClustering();
  virtual ~MapClustering();

  void setSaveIntermediateResults(const std::string &path);

  void setInputCloudBuildingInstances(const CloudPtr &cloud);
  void setInputCloudFenceInstances(const CloudPtr &cloud);
  void setInputCloudPoleInstances(const CloudPtr &cloud);
  void setInputCloudSignInstances(const CloudPtr &cloud);
  void setInputCloudTrunkInstances(const CloudPtr &cloud);
  void setInputCloudGroundInstances(const CloudPtr &cloud);

  void setInputCloudBuildingPrimitives(const CloudPtr &cloud);
  void setInputCloudFencePrimitives(const CloudPtr &cloud);
  void setInputCloudPolePrimitives(const CloudPtr &cloud);
  void setInputCloudSignPrimitives(const CloudPtr &cloud);
  void setInputCloudTrunkPrimitives(const CloudPtr &cloud);
  void setInputCloudGroundPrimitives(const CloudPtr &cloud);

  void clusterInstances(CloudPtr &cloud);
  void process();

  void processBuildings();
  void processFences();
  void processPoles();
  void processSigns();
  void processTrunks();
  void processGround();

  void saveVisualizedInstances(const std::string &path);
  void saveVisualizedPrimitives(const std::string &path);
  void saveVisualizedCells(const std::string &path);
  void rosPublisher(ros::NodeHandle &node,
                    bool segments =false,
                    bool instances = false,
                    bool primitives = false,
                    bool cells = false);
  void getVisualizedInstances(CloudRGBPtr &cloudRGB);
  void getVisualizedPrimitives(CloudRGBPtr &cloudRGB);
  void getVisualizedCells(CloudRGBPtr &cloudRGB);
  void setCellSize(float cellSize);

  void computeEaNdtFitnessScore(bool computeCompleteCloud = true);
  void computeNdtRepresentation(bool computeCompleteCloud = true);
  void computeNdtFitnessScore(bool computeCompleteCloud = true);
  void saveScores2CSV(const std::string &path) const;
  void reset();
  std::vector<pcl::octree::OctreeCovarianceContainer<PointT>*> getLeaves();

private:
  void saveResults(const std::string &filename, const std::map<std::string, std::vector<std::vector<double>>> &results) const;
  void saveScores(const std::string &filename, const std::map<std::string, std::vector<std::vector<float>>> &scores) const;
  void getHistogram(const std::vector<double> &scoreVector,
                    std::vector<unsigned int> &histogram,
                    std::vector<float> &bins,
                    const double &range,
                    const int &numBins);
  void computeEaNdtScore(const OctreeSegmentTPtr &octree, const CloudPtr &cloud, const std::string &id = "");
  void computeNdtScore(const OctreeGridTPtr &octree, const CloudPtr &cloud, const std::string &id = "");
  void printFormattedResults(const std::string &id, const double & score, const int &numLeaves, const double &time) const;
  void clusterVisualization(const CloudPtr &cloud,
                            const pcl::IndicesClustersPtr &clusters,
                            CloudRGBPtr &cloudRGB,
                            bool clear = true);
  void visualizePointsOutsideClusters(const CloudPtr &cloud, const pcl::IndicesClustersPtr &clusters, CloudRGBPtr &cloudRGB);
  void getClusters(const CloudPtr &cloud, pcl::IndicesClustersPtr &clusters);
  void cellClustering(const CloudPtr &cloud,
                      const pcl::IndicesClustersPtr &clusters,
                      CloudPtr &kmeans,
                      pcl::IndicesClustersPtr &kmeansClusters,
                      const int &instanceType,
                      const std::vector<float> &f,
                      float cellSize = 0.f);
  unsigned int getNumClusters(const CloudPtr &cloud,
                              const pcl::PointIndices &pointIndices,
                              const float &cellSize,
                              const int &method);
  unsigned int getNumClustersPlane(const CloudPtr &cloud, const pcl::PointIndices &pointIndices, const float &cellSize);
  unsigned int getNumClustersPole(const CloudPtr &cloud, const pcl::PointIndices &pointIndices, const float &cellSize);
  float estimatePlaneSize(const CloudPtr &cloud, const pcl::PointIndices &pointIndices, const float &leafSize = 0.1f);

  void addCells2EaNdtOctree(const CloudPtr &cloud,
                            const CloudPtr &kmeans,
                            const pcl::IndicesClustersPtr &kmeansClusters);

  void fillLabel(CloudPtr &cloud, pcl::IndicesClustersPtr &indicesClusters);

  void getVisualizedEaNdtCells(visualization_msgs::MarkerArray &markerArraySpheres) const;
  void getVisualizedEaNdtCells(const OctreeSegmentTPtr &octree,
                               visualization_msgs::MarkerArray &markerArraySpheres,
                               const std::vector<int> &RGB_spheres) const;
  void getVisualizedNdtCells(visualization_msgs::MarkerArray &markerArraySpheres,
                             visualization_msgs::Marker &markerLineList,
                             const std::vector<int> &RGB_spheres = {0, 255, 0},
                             const std::vector<int> &RGB_lines = {0, 255, 255}) const;

  std::string save_path_;

  CloudPtr cloud_;
  CloudPtr cloudBuilding_;
  CloudPtr cloudFence_;
  CloudPtr cloudPole_;
  CloudPtr cloudSign_;
  CloudPtr cloudTrunk_;
  CloudPtr cloudGround_;
  CloudPtr cloudPureGround_;
  
  pcl::IndicesClustersPtr buildingInstances_;
  pcl::IndicesClustersPtr fenceInstances_;
  pcl::IndicesClustersPtr poleInstances_;
  pcl::IndicesClustersPtr signInstances_;
  pcl::IndicesClustersPtr trunkInstances_;
  pcl::IndicesClustersPtr groundInstances_;
  
  pcl::IndicesClustersPtr buildingPrimitives_;
  pcl::IndicesClustersPtr fencePrimitives_;
  pcl::IndicesClustersPtr polePrimitives_;
  pcl::IndicesClustersPtr signPrimitives_;
  pcl::IndicesClustersPtr trunkPrimitives_;
  pcl::IndicesClustersPtr groundPrimitives_;

  pcl::IndicesClustersPtr buildingCells_;
  pcl::IndicesClustersPtr fenceCells_;
  pcl::IndicesClustersPtr poleCells_;
  pcl::IndicesClustersPtr signCells_;
  pcl::IndicesClustersPtr trunkCells_;
  pcl::IndicesClustersPtr groundCells_;
  pcl::IndicesClustersPtr groundSACSegmentedCells_;

  OctreeSegmentTPtr octreeEaNdt_;
  OctreeSegmentTPtr octreeEaNdtBuilding_;
  OctreeSegmentTPtr octreeEaNdtFence_;
  OctreeSegmentTPtr octreeEaNdtPole_;
  OctreeSegmentTPtr octreeEaNdtSign_;
  OctreeSegmentTPtr octreeEaNdtTrunk_;
  OctreeSegmentTPtr octreeEaNdtGround_;

  OctreeGridTPtr octreeNdt_;
  OctreeGridTPtr octreeNdtBuilding_;
  OctreeGridTPtr octreeNdtFence_;
  OctreeGridTPtr octreeNdtPole_;
  OctreeGridTPtr octreeNdtSign_;
  OctreeGridTPtr octreeNdtTrunk_;
  OctreeGridTPtr octreeNdtGround_;

  float cellSize_;
  std::map<std::string, std::vector<std::vector<double>>> resultsEaNdt_;
  std::map<std::string, std::vector<std::vector<double>>> resultsNdt_;

  std::map<std::string, std::vector<std::vector<float>>> scoreEaNdt_;
  std::map<std::string, std::vector<std::vector<float>>> scoreNdt_;
};


template <typename PointT>
MapClustering<PointT>::MapClustering()
: buildingInstances_ (new pcl::IndicesClusters),
  fenceInstances_ (new pcl::IndicesClusters),
  poleInstances_ (new pcl::IndicesClusters),
  signInstances_ (new pcl::IndicesClusters),
  trunkInstances_ (new pcl::IndicesClusters),
  groundInstances_ (new pcl::IndicesClusters),

  buildingPrimitives_ (new pcl::IndicesClusters),
  fencePrimitives_ (new pcl::IndicesClusters),
  polePrimitives_ (new pcl::IndicesClusters),
  signPrimitives_ (new pcl::IndicesClusters),
  trunkPrimitives_ (new pcl::IndicesClusters),
  groundPrimitives_ (new pcl::IndicesClusters),

  buildingCells_ (new pcl::IndicesClusters),
  fenceCells_ (new pcl::IndicesClusters),
  poleCells_ (new pcl::IndicesClusters),
  signCells_ (new pcl::IndicesClusters),
  trunkCells_ (new pcl::IndicesClusters),
  groundCells_ (new pcl::IndicesClusters),
  groundSACSegmentedCells_ (new pcl::IndicesClusters),

  cellSize_(1.0),
  save_path_ (""),

  octreeEaNdt_(new OctreeSegmentT(cellSize_/4.f)),
  octreeEaNdtBuilding_(new OctreeSegmentT(cellSize_/4.f)),
  octreeEaNdtFence_(new OctreeSegmentT(cellSize_/4.f)),
  octreeEaNdtPole_(new OctreeSegmentT(cellSize_/4.f)),
  octreeEaNdtSign_(new OctreeSegmentT(cellSize_/4.f)),
  octreeEaNdtTrunk_(new OctreeSegmentT(cellSize_/4.f)),
  octreeEaNdtGround_(new OctreeSegmentT(cellSize_/4.f)),

  octreeNdt_(new OctreeGridT(cellSize_)),
  octreeNdtBuilding_(new OctreeGridT(cellSize_)),
  octreeNdtFence_(new OctreeGridT(cellSize_)),
  octreeNdtPole_(new OctreeGridT(cellSize_)),
  octreeNdtSign_(new OctreeGridT(cellSize_)),
  octreeNdtTrunk_(new OctreeGridT(cellSize_)),
  octreeNdtGround_(new OctreeGridT(cellSize_)),

  cloud_ (new Cloud),
  cloudPureGround_ (new Cloud)
{
  cloudBuilding_ = nullptr;
  cloudFence_ = nullptr;
  cloudPole_ = nullptr;
  cloudSign_ = nullptr;
  cloudTrunk_ = nullptr;
  cloudGround_ = nullptr;
}


template <typename PointT>
MapClustering<PointT>::~MapClustering() {
  // TODO Auto-generated destructor stub
}


template <typename PointT>
void MapClustering<PointT>::setSaveIntermediateResults(const std::string &path) {
  save_path_ = path;
}

template <typename PointT>
void MapClustering<PointT>::clusterInstances(CloudPtr &cloud) {
  // Cluster instances
  std::cout << "\nClustering instances from semantic segments:\n";
  InstanceClustering<PointTD2L> clusterer;
  clusterer.setClusterTolerance(0.3f);
  clusterer.setMinClusterSize(10);
  clusterer.setInputCloud(cloud);
  clusterer.initSegments ();
  std::vector<Label> labels = {pole, traffic_sign, fence, trunk, building};
  clusterer.findInstanceClusters(labels);

  std::cout << "\nExtracting primitives from cluster instances:\n";
  // Initialize cluster clouds
  {
    CloudTD2LPtr buildingInstances (new CloudTD2L);
    clusterer.getCloud (building,     buildingInstances, buildingInstances_);
    setInputCloudBuildingInstances(buildingInstances);
    if (!save_path_.empty()) {
      std::string filename = save_path_ + "cloud_instances_building.pcd";
      pcl::io::savePCDFile<PointTD2L> (filename, *buildingInstances, true );
    }
  }
  {
    CloudTD2LPtr fenceInstances (new CloudTD2L);
    clusterer.getCloud (fence,        fenceInstances, fenceInstances_);
    setInputCloudFenceInstances(fenceInstances);
    if (!save_path_.empty()) {
      std::string filename = save_path_ + "cloud_instances_fence.pcd";
      pcl::io::savePCDFile<PointTD2L> (filename, *fenceInstances, true );
    }
  }
  {
    CloudTD2LPtr poleInstances (new CloudTD2L);
    clusterer.getCloud (pole,         poleInstances, poleInstances_);
    setInputCloudPoleInstances(poleInstances);
    if (!save_path_.empty()) {
      std::string filename = save_path_ + "cloud_instances_pole.pcd";
      pcl::io::savePCDFile<PointTD2L> (filename, *poleInstances, true );
    }
  }
  {
    CloudTD2LPtr signInstances (new CloudTD2L);
    clusterer.getCloud (traffic_sign, signInstances, signInstances_);
    setInputCloudSignInstances(signInstances);
    if (!save_path_.empty()) {
      std::string filename = save_path_ + "cloud_instances_sign.pcd";
      pcl::io::savePCDFile<PointTD2L> (filename, *signInstances, true );
    }
  }
  {
    CloudTD2LPtr trunkInstances (new CloudTD2L);
    clusterer.getCloud (trunk,        trunkInstances, trunkInstances_);
    setInputCloudTrunkInstances(trunkInstances);
    if (!save_path_.empty()) {
      std::string filename = save_path_ + "cloud_instances_trunk.pcd";
      pcl::io::savePCDFile<PointTD2L> (filename, *trunkInstances, true );
    }
  }
  {
    CloudTD2LPtr groundInstances (new CloudTD2L);
    clusterer.getCloud (ground,       groundInstances, groundInstances_);
    setInputCloudGroundInstances(groundInstances);
    if (!save_path_.empty()) {
      std::string filename = save_path_ + "cloud_instances_ground.pcd";
      pcl::io::savePCDFile<PointTD2L> (filename, *groundInstances, true );
    }
  }
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudBuildingInstances(const CloudPtr &cloud) {
  cloudBuilding_ = cloud;
  *cloud_ += *cloudBuilding_;

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(57) << std::left << "    Extracting primitives from building instances..." << std::flush;

  // Segment planes
  PlaneSegmentation<PointT> planeSegmenter;
  planeSegmenter.process(cloudBuilding_, buildingPrimitives_);

  if (!save_path_.empty()) {
    fillLabel(cloudBuilding_, buildingPrimitives_);
    std::string filename = save_path_ + "cloud_primitives_building.pcd";
    pcl::io::savePCDFile<PointTD2L> (filename, *cloudBuilding_, true );
  }

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << buildingPrimitives_->size() << " primitives found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudFenceInstances(const CloudPtr &cloud) {
  cloudFence_ = cloud;
  *cloud_ += *cloudFence_;

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(57) << std::left << "    Extracting primitives from fence instances..." << std::flush;

  // Segment planes
  PlaneSegmentation<PointT> planeSegmenter;
  planeSegmenter.process(cloudFence_, fencePrimitives_);

  if (!save_path_.empty()) {
    fillLabel(cloudFence_, fencePrimitives_);
    std::string filename = save_path_ + "cloud_primitives_fence.pcd";
    pcl::io::savePCDFile<PointTD2L> (filename, *cloudFence_, true );
  }

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << fencePrimitives_->size() << " primitives found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudPoleInstances(const CloudPtr &cloud) {
  cloudPole_ = cloud;
  *cloud_ += *cloudPole_;

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(57) << std::left << "    Extracting primitives from pole instances..." << std::flush;

  // Define clusters from cloud instance labels
  getClusters(cloudPole_, polePrimitives_);

  if (!save_path_.empty()) {
    fillLabel(cloudPole_, polePrimitives_);
    std::string filename = save_path_ + "cloud_primitives_pole.pcd";
    pcl::io::savePCDFile<PointTD2L> (filename, *cloudPole_, true );
  }

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << polePrimitives_->size() << " primitives found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudSignInstances(const CloudPtr &cloud) {
  cloudSign_ = cloud;
  *cloud_ += *cloudSign_;

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(57) << std::left << "    Extracting primitives from traffic sign instances..." << std::flush;

  // Define clusters from cloud instance labels
  getClusters(cloudSign_, signPrimitives_);

  if (!save_path_.empty()) {
    fillLabel(cloudSign_, signPrimitives_);
    std::string filename = save_path_ + "cloud_primitives_sign.pcd";
    pcl::io::savePCDFile<PointTD2L> (filename, *cloudSign_, true );
  }

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << signPrimitives_->size() << " primitives found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudTrunkInstances(const CloudPtr &cloud) {
  cloudTrunk_ = cloud;
  *cloud_ += *cloudTrunk_;

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(57) << std::left << "    Extracting primitives from tree trunk instances..." << std::flush;

  // Define clusters from cloud instance labels
  getClusters(cloudTrunk_, trunkPrimitives_);

  if (!save_path_.empty()) {
    fillLabel(cloudTrunk_, trunkPrimitives_);
    std::string filename = save_path_ + "cloud_primitives_trunk.pcd";
    pcl::io::savePCDFile<PointTD2L> (filename, *cloudTrunk_, true );
  }

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << trunkPrimitives_->size() << " primitives found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudGroundInstances(const CloudPtr &cloud) {
  cloudGround_ = cloud;

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(57) << std::left << "    Extracting primitives from ground instances..." << std::flush;

  // Euclidean clustering to remove majority of noise below ground plane resulting from reflecting surfaces
  KdTreeTPtr tree (new KdTreeT);
  tree->setInputCloud (cloudGround_);

  std::vector<pcl::PointIndices> cluster_indices;
  pcl::EuclideanClusterExtraction<PointT> ec;
  ec.setClusterTolerance (0.5); // 50cm
  ec.setMinClusterSize (3000);
  ec.setSearchMethod (tree);
  ec.setInputCloud (cloudGround_);
  ec.extract (cluster_indices);

  // Combine all clusters found by the Euclidean clustering
  pcl::IndicesPtr indicesPtr(new pcl::Indices);
  for (auto &cluster : cluster_indices)
    indicesPtr->insert(indicesPtr->end(), cluster.indices.begin(), cluster.indices.end());

  // Extract found indices
  pcl::ExtractIndices<PointT> filter;
  filter.setInputCloud (cloudGround_);
  filter.setIndices (indicesPtr);
  filter.filter (*cloudGround_);

  // Add filtered ground points in the complete cloud
  *cloud_ += *cloudGround_;

  // Approximate size of the ground
  CloudPtr cloud_voxel_filtered (new Cloud);
  pcl::VoxelGrid<PointT> voxelGrid;
  voxelGrid.setInputCloud (cloudGround_);
  voxelGrid.setLeafSize (1.0f, 1.0f, std::numeric_limits<float>::max());
  voxelGrid.filter (*cloud_voxel_filtered);
  float groundSize = float(cloud_voxel_filtered->size()) * pow(cellSize_, 2); // m²

  // Define number of clusters so that each cluster will be approximately 100 m²
  float clusterSize = 100.0; // m²
  int numClusters = std::ceil( groundSize / clusterSize );

  Kmeans<PointT> kmeansExtractor;
  kmeansExtractor.setInputCloud (cloud_voxel_filtered);
  kmeansExtractor.process2D(numClusters);

  CloudPtr means (new Cloud);
  kmeansExtractor.getMeans(means);

  KdTreeTPtr meansTree (new KdTreeT);
  meansTree->setInputCloud(means);

  groundPrimitives_->resize(means->size());
  for (int pointIdx = 0; pointIdx < cloudGround_->points.size(); pointIdx++) {
    pcl::Indices k_indices;
    std::vector<float> k_sqr_distances;
    meansTree->nearestKSearch (cloudGround_->points[pointIdx], 1, k_indices, k_sqr_distances);
    groundPrimitives_->at(k_indices[0]).indices.push_back(pointIdx);
  }

  PlaneSegmentation<PointT> planeSegmenter;
  planeSegmenter.segmentGroundPlane(cloudGround_, groundPrimitives_, 0.3);

  if (!save_path_.empty()) {
    fillLabel(cloudGround_, groundPrimitives_);
    std::string filename = save_path_ + "cloud_primitives_ground.pcd";
    pcl::io::savePCDFile<PointTD2L> (filename, *cloudGround_, true );
  }

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << groundPrimitives_->size() << " primitives found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudBuildingPrimitives(const CloudPtr &cloud) {
  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(44) << std::left << "    Initializing building primitives..." << std::flush;

  cloudBuilding_ = cloud;
  *cloud_ += *cloudBuilding_;
  getClusters(cloudBuilding_, buildingPrimitives_);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << buildingPrimitives_->size() << " primitives initialized.\n";
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudFencePrimitives(const CloudPtr &cloud) {
  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(44) << std::left << "    Initializing fence primitives..." << std::flush;

  cloudFence_ = cloud;
  *cloud_ += *cloudFence_;
  getClusters(cloudFence_, fencePrimitives_);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << fencePrimitives_->size() << " primitives initialized.\n";
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudPolePrimitives(const CloudPtr &cloud) {
  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(44) << std::left << "    Initializing pole primitives..." << std::flush;

  cloudPole_ = cloud;
  *cloud_ += *cloudPole_;
  getClusters(cloudPole_, polePrimitives_);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << polePrimitives_->size() << " primitives initialized.\n";
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudSignPrimitives(const CloudPtr &cloud) {
  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(44) << std::left << "    Initializing traffic sign primitives..." << std::flush;

  cloudSign_ = cloud;
  *cloud_ += *cloudSign_;
  getClusters(cloudSign_, signPrimitives_);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << signPrimitives_->size() << " primitives initialized.\n";
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudTrunkPrimitives(const CloudPtr &cloud) {
  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(44) << std::left << "    Initializing tree trunk primitives..." << std::flush;

  cloudTrunk_ = cloud;
  *cloud_ += *cloudTrunk_;
  getClusters(cloudTrunk_, trunkPrimitives_);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << trunkPrimitives_->size() << " primitives initialized.\n";
}


template <typename PointT>
void
MapClustering<PointT>::setInputCloudGroundPrimitives(const CloudPtr &cloud) {
  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << std::setw(44) << std::left << "    Initializing ground primitives..." << std::flush;

  cloudGround_ = cloud;
  *cloud_ += *cloudGround_;
  getClusters(cloudGround_, groundPrimitives_);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(4) << groundPrimitives_->size() << " primitives initialized.\n";
}


template <typename PointT>
void
MapClustering<PointT>::process() {
  std::cout << "\nClustering cells with cell size of "
            << std::fixed << std::setprecision(2) << cellSize_ << " meters:" << std::endl;

  processBuildings();
  processFences();
  processPoles();
  processSigns();
  processTrunks();
  processGround();
}


template <typename PointT>
void
MapClustering<PointT>::setCellSize(float cellSize) {
  cellSize_ = cellSize;

  // Create resolution of octree smaller than the actual resolution to ensure that all clusters fit the tree
  octreeEaNdt_->setResolution(cellSize_/4.f);
  octreeEaNdtBuilding_->setResolution(cellSize_/4.f);
  octreeEaNdtFence_->setResolution(cellSize_/4.f);
  octreeEaNdtPole_->setResolution(cellSize_/4.f);
  octreeEaNdtSign_->setResolution(cellSize_/4.f);
  octreeEaNdtTrunk_->setResolution(cellSize_/4.f);
  octreeEaNdtGround_->setResolution(cellSize_/4.f);

  octreeNdt_->setResolution(cellSize_);
  octreeNdtBuilding_->setResolution(cellSize_);
  octreeNdtFence_->setResolution(cellSize_);
  octreeNdtPole_->setResolution(cellSize_);
  octreeNdtSign_->setResolution(cellSize_);
  octreeNdtTrunk_->setResolution(cellSize_);
  octreeNdtGround_->setResolution(cellSize_);
}


template <typename PointT>
void
MapClustering<PointT>::reset() {

  buildingCells_->clear();
  fenceCells_->clear();
  poleCells_->clear();
  signCells_->clear();
  trunkCells_->clear();
  groundCells_->clear();

  octreeEaNdt_->deleteTree();
  octreeEaNdtBuilding_->deleteTree();
  octreeEaNdtFence_->deleteTree();
  octreeEaNdtPole_->deleteTree();
  octreeEaNdtSign_->deleteTree();
  octreeEaNdtTrunk_->deleteTree();
  octreeEaNdtGround_->deleteTree();

  octreeNdt_->deleteTree();
  octreeNdtBuilding_->deleteTree();
  octreeNdtFence_->deleteTree();
  octreeNdtPole_->deleteTree();
  octreeNdtSign_->deleteTree();
  octreeNdtTrunk_->deleteTree();
  octreeNdtGround_->deleteTree();
}


template <typename PointT>
void
MapClustering<PointT>::addCells2EaNdtOctree(const CloudPtr &cloud,
                                          const CloudPtr &kmeans,
                                          const pcl::IndicesClustersPtr &kmeansClusters) {

  for (int i = 0; i < kmeansClusters->size(); i++) {
    pcl::IndicesPtr indicesPtr (new pcl::Indices);
    *indicesPtr = kmeansClusters->at(i).indices;

    octreeEaNdt_->setInputCloud(cloud, indicesPtr);
    octreeEaNdt_->addPointsFromInputCloud(kmeans->points[i]);
  }
}


template <typename PointT>
void
MapClustering<PointT>::getClusters(const CloudPtr &cloud, pcl::IndicesClustersPtr &clusters) {
  for (int index = 0; index < cloud->size(); index++) {
    int inst_label = cloud->points[index].inst_label;
    if (inst_label == std::numeric_limits<uint16_t>::max())
      continue;
    if (clusters->size() <= inst_label)
      clusters->resize(inst_label + 1);
    clusters->at(inst_label).indices.push_back(index);
  }
}


template <typename PointT>
void
MapClustering<PointT>::cellClustering(const CloudPtr &cloud,
                                        const pcl::IndicesClustersPtr &clusters,
                                        CloudPtr &kmeans,
                                        pcl::IndicesClustersPtr &kmeansClusters,
                                        const int &instanceType,
                                        const std::vector<float> &f,
                                        float cellSize) {
  if (cellSize == 0.f)
    cellSize = cellSize_;

  for (auto &pointIndices : *clusters) {

    unsigned int numClusters = getNumClusters(cloud, pointIndices, cellSize, instanceType);
    numClusters = ceil(f[0] * pow(cellSize, f[1]) * float(numClusters));

    if (!numClusters)
      continue;

    pcl::IndicesPtr indices(new pcl::Indices);
    *indices = pointIndices.indices;
    Kmeans<PointT> kmeansExtractor;
    kmeansExtractor.setInputCloud(cloud, indices);
    kmeansExtractor.process(numClusters);

    CloudPtr means (new Cloud);
    pcl::IndicesClustersPtr subClusters (new pcl::IndicesClusters);
    kmeansExtractor.getMeans(means);
    kmeansExtractor.getClusters(subClusters);
    *kmeans += *means;
    kmeansClusters->insert( kmeansClusters->end(), subClusters->begin(), subClusters->end() );
  }
}


template <typename PointT>
unsigned int
MapClustering<PointT>::getNumClusters(const CloudPtr &cloud,
                                      const pcl::PointIndices &pointIndices,
                                      const float &cellSize,
                                      const int &method) {
  if (method == -1) // elongated clusters
    return getNumClustersPole(cloud, pointIndices, cellSize);
  else if (method == -2) // planar clusters
    return getNumClustersPlane(cloud, pointIndices, cellSize);
  else  // use the number of clusters
    return method;
}

template <typename PointT>
unsigned int
MapClustering<PointT>::getNumClustersPole(const CloudPtr &cloud, const pcl::PointIndices &pointIndices, const float &cellSize) {
  Eigen::Vector4f min_pt, max_pt;
  pcl::getMinMax3D (*cloud, pointIndices, min_pt, max_pt);
  Eigen::Vector3f diff = (max_pt - min_pt).head(3);

  return std::ceil(diff.norm() / cellSize);
}


template <typename PointT>
unsigned int
MapClustering<PointT>::getNumClustersPlane(const CloudPtr &cloud, const pcl::PointIndices &pointIndices, const float &cellSize) {
  float planeSize = estimatePlaneSize(cloud, pointIndices);

  // Define number of clusters
  float clusterSize = pow(cellSize, 2); // m²
  return std::ceil( planeSize / clusterSize );
}


template <typename PointT>
float
MapClustering<PointT>::estimatePlaneSize(const CloudPtr &cloud,
                                         const pcl::PointIndices &pointIndices,
                                         const float &leafSize) {
  if (pointIndices.indices.size() < 3)
    return 0;

  pcl::IndicesPtr indices(new pcl::Indices);
  *indices = pointIndices.indices;

  pcl::PCA<PointT> pca;
  pca.setInputCloud(cloud);
  pca.setIndices(indices);
  Eigen::Matrix4f tf = Eigen::Matrix4f::Identity();
  tf.block<3, 3>(0, 0) = pca.getEigenVectors().transpose();

  CloudPtr projectedCloud (new Cloud);
  pcl::transformPointCloud(*cloud, pointIndices, *projectedCloud, tf);

  // Estimate surface of the plane
  CloudPtr cloud_filtered (new Cloud);
  pcl::VoxelGrid<PointT> voxelGrid;
  voxelGrid.setInputCloud (projectedCloud);
  voxelGrid.setLeafSize (leafSize, leafSize, std::numeric_limits<float>::max());
  voxelGrid.filter (*cloud_filtered);
  float planeSize = float(cloud_filtered->size()) * pow(leafSize, 2); // m²

  return planeSize;
}


template <typename PointT>
void
MapClustering<PointT>::processBuildings() {

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << "    " << std::setw(49) << std::left << "Clustering cells from building primitives..." << std::flush;

  // Cluster each plane with Kmeans++
  CloudPtr kmeans (new Cloud);
  std::vector<float> f = {2.7078758377808536, 0.13722034139500836};
  cellClustering(cloudBuilding_, buildingPrimitives_, kmeans, buildingCells_, -2, f);

  // Add clusters into octrees
  octreeEaNdt_->setInputCloud(cloudBuilding_);
  octreeEaNdt_->addClustersFromInputCloud(buildingCells_, kmeans);
  octreeEaNdtBuilding_->setInputCloud(cloudBuilding_);
  octreeEaNdtBuilding_->addClustersFromInputCloud(buildingCells_, kmeans);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << std::right << buildingCells_->size() << " cells found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::processFences() {

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << "    " << std::setw(49) << std::left << "Clustering cells from fence primitives..." << std::flush;

  // Cluster each plane with Kmeans++
  CloudPtr kmeans (new Cloud);
  std::vector<float> f = {4.17948650640806, 0.31843996435228533};
  cellClustering(cloudFence_, fencePrimitives_, kmeans, fenceCells_, -2, f);

  // Add clusters into octrees
  octreeEaNdt_->setInputCloud(cloudFence_);
  octreeEaNdt_->addClustersFromInputCloud(fenceCells_, kmeans);
  octreeEaNdtFence_->setInputCloud(cloudFence_);
  octreeEaNdtFence_->addClustersFromInputCloud(fenceCells_, kmeans);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << std::right << fenceCells_->size() << " cells found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::processPoles() {

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << "    " << std::setw(49) << std::left << "Clustering cells from pole primitives..." << std::flush;

  // Cluster each pole with Kmeans++
  CloudPtr kmeans (new Cloud);
  pcl::IndicesClustersPtr kmeansClusters (new pcl::IndicesClusters);
  std::vector<float> f = {1.6874096382321715, -0.31506643695059683};
  cellClustering(cloudPole_, polePrimitives_, kmeans, poleCells_, -1, f);

  // Add clusters into octree
  octreeEaNdt_->setInputCloud(cloudPole_);
  octreeEaNdt_->addClustersFromInputCloud(poleCells_, kmeans);
  octreeEaNdtPole_->setInputCloud(cloudPole_);
  octreeEaNdtPole_->addClustersFromInputCloud(poleCells_, kmeans);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << poleCells_->size() << " cells found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::processSigns() {

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << "    " << std::setw(49) << std::left << "Clustering cells from traffic sign primitives..." << std::flush;

  // Cluster each sign with Kmeans++
  CloudPtr kmeans (new Cloud);
  std::vector<float> f = {3.9231919696267386, 0.3165458127096211};
  cellClustering(cloudSign_, signPrimitives_, kmeans, signCells_, -2, f);

  // Add clusters into octree
  octreeEaNdt_->setInputCloud(cloudSign_);
  octreeEaNdt_->addClustersFromInputCloud(signCells_, kmeans);
  octreeEaNdtSign_->setInputCloud(cloudSign_);
  octreeEaNdtSign_->addClustersFromInputCloud(signCells_, kmeans);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << std::right << signCells_->size() << " cells found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::processTrunks() {

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << "    " << std::setw(49) << std::left << "Clustering cells from tree trunk primitives..." << std::flush;

  // Cluster each trunk with Kmeans++
  CloudPtr kmeans (new Cloud);
  std::vector<float> f = {2.2479127883095584, -0.7883008443523578};
  cellClustering(cloudTrunk_, trunkPrimitives_, kmeans, trunkCells_, -1, f);

  // Add clusters into octree
  octreeEaNdt_->setInputCloud(cloudTrunk_);
  octreeEaNdt_->addClustersFromInputCloud(trunkCells_, kmeans);
  octreeEaNdtTrunk_->setInputCloud(cloudTrunk_);
  octreeEaNdtTrunk_->addClustersFromInputCloud(trunkCells_, kmeans);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << std::right << trunkCells_->size() << " cells found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::processGround() {

  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << "    " << std::setw(49) << std::left << "Clustering cells from ground primitives..." << std::flush;

  // Cluster each ground patch with Kmeans++
  CloudPtr kmeans (new Cloud);
  std::vector<float> f = {1.6803924146591254, 0.08305231866698243};
  cellClustering(cloudGround_, groundPrimitives_, kmeans, groundCells_, -2, f);

  // Segment planes
  *groundSACSegmentedCells_ = *groundCells_;
  PlaneSegmentation<PointT> planeSegmenter;
  planeSegmenter.segmentGroundPlane(cloudGround_, groundSACSegmentedCells_, 0.15);

  // Add clusters into octree
  octreeEaNdt_->setInputCloud(cloudGround_);
  octreeEaNdt_->addClustersFromInputCloud(groundSACSegmentedCells_, kmeans);
  octreeEaNdtGround_->setInputCloud(cloudGround_);
  octreeEaNdtGround_->addClustersFromInputCloud(groundSACSegmentedCells_, kmeans);

  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << std::right << groundSACSegmentedCells_->size() << " cells found.\n";
}


template <typename PointT>
void
MapClustering<PointT>::computeEaNdtFitnessScore(bool computeCompleteCloud) {

  std::cout << "\nComputing score for EA-NDT representation with cell size of "
            << std::fixed << std::setprecision(2) << cellSize_ << " meters:" << std::endl;
  if (computeCompleteCloud)
    computeEaNdtScore(octreeEaNdt_,  cloud_,             "Complete");
  computeEaNdtScore(octreeEaNdtBuilding_, cloudBuilding_, "Building");
  computeEaNdtScore(octreeEaNdtFence_,    cloudFence_,    "Fence");
  computeEaNdtScore(octreeEaNdtPole_,     cloudPole_,     "Pole");
  computeEaNdtScore(octreeEaNdtSign_,     cloudSign_,     "Sign");
  computeEaNdtScore(octreeEaNdtTrunk_,    cloudTrunk_,    "Trunk");
  computeEaNdtScore(octreeEaNdtGround_,   cloudGround_,   "Ground");
}


template <typename PointT>
void
MapClustering<PointT>::computeEaNdtScore(const OctreeSegmentTPtr &octree, const CloudPtr &cloud, const std::string &id) {
  if (octree->getLeafCount() < 1)
    return;

  OctreeMap<OctreeSegmentTPtr, PointT> octreeMap(octree, cellSize_);

  // Compute score
  pcl::console::TicToc tt;
  tt.tic ();
  std::vector<float> scores;
  double score = octreeMap.computeFitnessScore(cloud, scores);
  double time = tt.toc () / 1e3;

  // Get leaves
  int numLeaves = octree->getLeafCount();

  // Save result
  if ( resultsEaNdt_.find(id) == resultsEaNdt_.end() ) {
    resultsEaNdt_[id] = std::vector<std::vector<double>>();
    scoreEaNdt_[id] = std::vector<std::vector<float>>();
  }

  std::vector<double> result {cellSize_, numLeaves, score, time};
  resultsEaNdt_[id].push_back(result);
  scoreEaNdt_[id].push_back(scores);

  printFormattedResults(id, score, numLeaves, time);
}


template <typename PointT>
void
MapClustering<PointT>::computeNdtRepresentation(bool computeCompleteCloud) {

  pcl::console::TicToc tt;
  std::cout << "\nComputing NDT representations with cell size of "
            << std::fixed << std::setprecision(2) << cellSize_ << " meters:" << std::endl;

  if (computeCompleteCloud) {
    tt.tic ();
    std::cout << "    " << std::setw(53) << std::left << "Computing complete NDT representation..." << std::flush;
    octreeNdt_->setInputCloud(cloud_);
    octreeNdt_->addPointsFromInputCloud();
    std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
              << " sec, " << std::setw(6) << std::right << octreeNdt_->getLeafCount() << " cells created.\n";
  }

  tt.tic ();
  std::cout << "    " << std::setw(53) << std::left << "Computing NDT representation for building segment..." << std::flush;
  octreeNdtBuilding_->setInputCloud(cloudBuilding_);
  octreeNdtBuilding_->addPointsFromInputCloud();
  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << std::right << octreeNdtBuilding_->getLeafCount() << " cells created.\n";


  tt.tic ();
  std::cout << "    " << std::setw(53) << std::left << "Computing NDT representation for fence segment..." << std::flush;
  octreeNdtFence_->setInputCloud(cloudFence_);
  octreeNdtFence_->addPointsFromInputCloud();
  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << std::right << octreeNdtFence_->getLeafCount() << " cells created.\n";

  tt.tic ();
  std::cout << "    " << std::setw(53) << std::left << "Computing NDT representation for pole segment..." << std::flush;
  octreeNdtPole_->setInputCloud(cloudPole_);
  octreeNdtPole_->addPointsFromInputCloud();
  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << std::right << octreeNdtPole_->getLeafCount() << " cells created.\n";

  tt.tic ();
  std::cout << "    " << std::setw(53) << std::left << "Computing NDT representation for sign segment..." << std::flush;
  octreeNdtSign_->setInputCloud(cloudSign_);
  octreeNdtSign_->addPointsFromInputCloud();
  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << std::right << octreeNdtSign_->getLeafCount() << " cells created.\n";

  tt.tic ();
  std::cout << "    " << std::setw(53) << std::left << "Computing NDT representation for trunk segment..." << std::flush;
  octreeNdtTrunk_->setInputCloud(cloudTrunk_);
  octreeNdtTrunk_->addPointsFromInputCloud();
  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << std::right << octreeNdtTrunk_->getLeafCount() << " cells created.\n";

  tt.tic ();
  std::cout << "    " << std::setw(53) << std::left << "Computing NDT representation for ground segment..." << std::flush;
  octreeNdtGround_->setInputCloud(cloudGround_);
  octreeNdtGround_->addPointsFromInputCloud();
  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
            << " sec, " << std::setw(6) << std::right << octreeNdtGround_->getLeafCount() << " cells created.\n";
}


template <typename PointT>
void
MapClustering<PointT>::computeNdtFitnessScore(bool computeCompleteCloud) {

  std::cout << "\nComputing score for NDT representation with cell size of "
            << std::fixed << std::setprecision(2) << cellSize_ << " meters:" << std::endl;
  if (computeCompleteCloud) {
    octreeNdt_->setInputCloud(cloud_);
    octreeNdt_->addPointsFromInputCloud();
    computeNdtScore(octreeNdt_, cloud_, "Complete");
  }

  computeNdtScore(octreeNdtBuilding_, cloudBuilding_, "Building");
  computeNdtScore(octreeNdtFence_, cloudFence_, "Fence");
  computeNdtScore(octreeNdtPole_, cloudPole_, "Pole");
  computeNdtScore(octreeNdtSign_, cloudSign_, "Sign");
  computeNdtScore(octreeNdtTrunk_, cloudTrunk_, "Trunk");
  computeNdtScore(octreeNdtGround_, cloudGround_, "Ground");
}


template <typename PointT>
void
MapClustering<PointT>::computeNdtScore(const OctreeGridTPtr &octree, const CloudPtr &cloud, const std::string &id) {
  if (octree->getLeafCount() < 1)
    return;

  OctreeMap<OctreeGridTPtr, PointT> octreeMap(octree);

  // Compute score
  pcl::console::TicToc tt;
  tt.tic ();
  std::vector<float> scores;
  double score = octreeMap.computeFitnessScore(cloud, scores);
  double time = tt.toc () / 1e3;

  // Get leaves
  int numLeaves = octree->getLeafCount();

  // Save result
  if ( resultsNdt_.find(id) == resultsNdt_.end() ) {
    resultsNdt_[id] = std::vector<std::vector<double>>();
    scoreNdt_[id] = std::vector<std::vector<float>>();
  }

  std::vector<double> result {cellSize_, numLeaves, score, time};
  resultsNdt_[id].push_back(result);
  scoreNdt_[id].push_back(scores);

  printFormattedResults(id, score, numLeaves, time);
}


template <typename PointT>
void
MapClustering<PointT>::printFormattedResults(const std::string &id, const double & score, const int &numLeaves, const double &time) const {
  // Print results in terminal
  std::cout << "    " << std::setw(10) << std::left << std::fixed << std::setprecision(3) << id
                      << "score: " << std::setw(7) << score
                      << "leaves: " << std::setw(6) << std::right << numLeaves
                      << std::setprecision(3) << ", " << time << " sec\n";
}


template <typename PointT>
void
MapClustering<PointT>::saveScores2CSV(const std::string &path) const {

  saveResults (path + "ndt_results.csv", resultsNdt_);
  saveResults (path + "ea_ndt_results.csv", resultsEaNdt_);

  saveScores (path + "ndt_scores.bin", scoreNdt_);
  saveScores (path + "ea_ndt_scores.bin", scoreEaNdt_);
}


template <typename PointT>
void
MapClustering<PointT>::saveResults(const std::string &filename,
                                   const std::map<std::string, std::vector<std::vector<double>>> &results) const {
  std::ofstream file;
  file << std::fixed;
  file.open(filename);

  for (const auto &key : results) {
    for (const auto &result : key.second) {
      file << std::left
           << std::setw(14) << key.first
           << std::right
           << std::setprecision(3) << std::setw(6) << result[0] << "  "
           << std::setprecision(0) << std::setw(7) << result[1] << "  "
           << std::setprecision(4) << std::setw(7) << result[2] << "  "
           << std::setprecision(3) << std::setw(7) << result[3] << "  " << std::endl;
    }
  }

  file.close();
}


template <typename PointT>
void
MapClustering<PointT>::saveScores(const std::string &filename,
                                  const std::map<std::string, std::vector<std::vector<float>>> &scores) const {

  // Write the amount of labels as first thing in the file
  unsigned int mapSize = scores.size();
  std::ofstream file (filename, ios::out | ios::binary);
  file.write((char*)&mapSize, sizeof(mapSize));

  // Write number of cell sizes for each semantic segment
  for (const auto &key : scores) {
    unsigned int numCellSize = key.second.size();
    file.write((char*)&numCellSize, sizeof(numCellSize));
  }

  // Write the length of score vector for each cell size
  for (const auto &key : scores) {
    for (const auto &resScoreVec : key.second) {
      unsigned int numScores = resScoreVec.size();
      file.write((char*)&numScores, sizeof(numScores));
    }
  }

  // Write the histogram vectors in the end of the file
  for (const auto &key : scores) {
    for (const auto &resScoreVec : key.second) {
      unsigned int numScores = resScoreVec.size();
      file.write((char*)&resScoreVec[0], resScoreVec.size() * sizeof(float));
    }
  }

  file.close();
}


template <typename PointT>
void
MapClustering<PointT>::getHistogram(const std::vector<double> &scoreVector,
                                    std::vector<unsigned int> &histogram,
                                    std::vector<float> &bins,
                                    const double &range,
                                    const int &numBins) {

  //double range = std::max_element(scoreVector.begin(), scoreVector.end());
  double bucketSize = range / numBins;

  bins = {-2.5, -1.5};
  for (int i = 0; i < numBins; i++)
    bins.push_back(bucketSize * i);

  histogram.resize(numBins + 2, 0);
  for (auto &score : scoreVector) {
    int binIdx = (int)floor(score / bucketSize) + 2;
    histogram[binIdx]++;
  }
}


template <typename PointT>
void
MapClustering<PointT>::saveVisualizedInstances(const std::string &path) {

  CloudRGBPtr cloudRGB (new CloudRGB);

  if (cloudBuilding_) {
    clusterVisualization(cloudBuilding_, buildingInstances_, cloudRGB);
    visualizePointsOutsideClusters(cloudBuilding_, buildingInstances_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_instances_building.pcd", *cloudRGB);
  }
  if (cloudFence_) {
    clusterVisualization(cloudFence_, fenceInstances_, cloudRGB);
    visualizePointsOutsideClusters(cloudFence_, fenceInstances_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_instances_fence.pcd", *cloudRGB);
  }
  if (cloudPole_) {
    clusterVisualization(cloudPole_, poleInstances_, cloudRGB);
    visualizePointsOutsideClusters(cloudPole_, poleInstances_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_instances_pole.pcd", *cloudRGB);
  }
  if (cloudSign_) {
    clusterVisualization(cloudSign_, signInstances_, cloudRGB);
    visualizePointsOutsideClusters(cloudSign_, signInstances_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_instances_sign.pcd", *cloudRGB);
  }
  if (cloudTrunk_) {
    clusterVisualization(cloudTrunk_, trunkInstances_, cloudRGB);
    visualizePointsOutsideClusters(cloudTrunk_, trunkInstances_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_instances_trunk.pcd", *cloudRGB);
  }
  if (cloudGround_) {
    clusterVisualization(cloudGround_, groundInstances_, cloudRGB);
    visualizePointsOutsideClusters(cloudGround_, groundInstances_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_instances_ground.pcd", *cloudRGB);
  }

}


template <typename PointT>
void
MapClustering<PointT>::saveVisualizedPrimitives(const std::string &path) {

  CloudRGBPtr cloudRGB (new CloudRGB);

  if (cloudBuilding_) {
    clusterVisualization(cloudBuilding_, buildingPrimitives_, cloudRGB);
    visualizePointsOutsideClusters(cloudBuilding_, buildingPrimitives_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_primitives_building.pcd", *cloudRGB);
  }
  if (cloudFence_) {
    clusterVisualization(cloudFence_, fencePrimitives_, cloudRGB);
    visualizePointsOutsideClusters(cloudFence_, fencePrimitives_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_primitives_fence.pcd", *cloudRGB);
  }
  if (cloudPole_) {
    clusterVisualization(cloudPole_, polePrimitives_, cloudRGB);
    visualizePointsOutsideClusters(cloudPole_, polePrimitives_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_primitives_pole.pcd", *cloudRGB);
  }
  if (cloudSign_) {
    clusterVisualization(cloudSign_, signPrimitives_, cloudRGB);
    visualizePointsOutsideClusters(cloudSign_, signPrimitives_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_primitives_sign.pcd", *cloudRGB);
  }
  if (cloudTrunk_) {
    clusterVisualization(cloudTrunk_, trunkPrimitives_, cloudRGB);
    visualizePointsOutsideClusters(cloudTrunk_, trunkPrimitives_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_primitives_trunk.pcd", *cloudRGB);
  }
  if (cloudGround_) {
    clusterVisualization(cloudGround_, groundPrimitives_, cloudRGB);
    visualizePointsOutsideClusters(cloudGround_, groundPrimitives_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_primitives_ground.pcd", *cloudRGB);
  }
}


template <typename PointT>
void
MapClustering<PointT>::saveVisualizedCells(const std::string &path) {

  CloudRGBPtr cloudRGB (new CloudRGB);
  std::ostringstream cell_size_ss;
  cell_size_ss << std::fixed << std::setprecision(2) << cellSize_;

  if (cloudBuilding_) {
    clusterVisualization(cloudBuilding_, buildingCells_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_cells_size_" + cell_size_ss.str() + "_building.pcd", *cloudRGB);
  }
  if (cloudFence_) {
    clusterVisualization(cloudFence_, fenceCells_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_cells_size_" + cell_size_ss.str() + "_fence.pcd", *cloudRGB);
  }
  if (cloudPole_) {
    clusterVisualization(cloudPole_, poleCells_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_cells_size_" + cell_size_ss.str() + "_pole.pcd", *cloudRGB);
  }
  if (cloudSign_) {
    clusterVisualization(cloudSign_, signCells_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_cells_size_" + cell_size_ss.str() + "_sign.pcd", *cloudRGB);
  }
  if (cloudTrunk_) {
    clusterVisualization(cloudTrunk_, trunkCells_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_cells_size_" + cell_size_ss.str() + "_trunk.pcd", *cloudRGB);
  }
  if (cloudGround_) {
    clusterVisualization(cloudGround_, groundSACSegmentedCells_, cloudRGB);
    pcl::io::savePCDFile<PointRGB> (path + "RGB_cloud_cells_size_" + cell_size_ss.str() + "_ground.pcd", *cloudRGB);
  }
}


template <typename PointT>
void
MapClustering<PointT>::getVisualizedInstances(CloudRGBPtr &cloudRGB) {

  clusterVisualization(cloudBuilding_, buildingInstances_, cloudRGB, false);
  clusterVisualization(cloudFence_,    fenceInstances_,    cloudRGB, false);
  clusterVisualization(cloudPole_,     poleInstances_,     cloudRGB, false);
  clusterVisualization(cloudSign_,     signInstances_,     cloudRGB, false);
  clusterVisualization(cloudTrunk_,    trunkInstances_,    cloudRGB, false);
  clusterVisualization(cloudGround_,   groundInstances_,   cloudRGB, false);
}

template <typename PointT>
void
MapClustering<PointT>::getVisualizedPrimitives(CloudRGBPtr &cloudRGB) {

  clusterVisualization(cloudBuilding_, buildingPrimitives_, cloudRGB, false);
  clusterVisualization(cloudFence_,    fencePrimitives_,    cloudRGB, false);
  clusterVisualization(cloudPole_,     polePrimitives_,     cloudRGB, false);
  clusterVisualization(cloudSign_,     signPrimitives_,     cloudRGB, false);
  clusterVisualization(cloudTrunk_,    trunkPrimitives_,    cloudRGB, false);
  clusterVisualization(cloudGround_,   groundPrimitives_,   cloudRGB, false);
}

template <typename PointT>
void
MapClustering<PointT>::getVisualizedCells(CloudRGBPtr &cloudRGB) {

  clusterVisualization(cloudBuilding_, buildingCells_, cloudRGB, false);
  clusterVisualization(cloudFence_,    fenceCells_,    cloudRGB, false);
  clusterVisualization(cloudPole_,     poleCells_,     cloudRGB, false);
  clusterVisualization(cloudSign_,     signCells_,     cloudRGB, false);
  clusterVisualization(cloudTrunk_,    trunkCells_,    cloudRGB, false);
  clusterVisualization(cloudGround_,   groundSACSegmentedCells_,   cloudRGB, false);
}


template <typename PointT>
void
MapClustering<PointT>::clusterVisualization(const CloudPtr &cloud,
                                            const pcl::IndicesClustersPtr &clusters,
                                            CloudRGBPtr &cloudRGB,
                                            bool clear) {

  if (clear)
    cloudRGB->clear();

  for (auto &pointIndices : *clusters) {
    /*  Generate RGB values such that
     *  one of the values cannot be below 128
     *  and two of the values cannot exceed 223
     */
    std::vector<float> r0{0, 0, 1}, r1{0, 1, 1};
    auto rng = std::default_random_engine {};
    std::shuffle(std::begin(r0), std::end(r0), rng);
    std::shuffle(std::begin(r1), std::end(r1), rng);

    int r = int(r0[0] * 128.f + (255.99f - r0[2] * 128.f - r1[2] * 32.f) * rand() / RAND_MAX);
    int g = int(r0[0] * 128.f + (255.99f - r0[2] * 128.f - r1[2] * 32.f) * rand() / RAND_MAX);
    int b = int(r0[0] * 128.f + (255.99f - r0[2] * 128.f - r1[2] * 32.f) * rand() / RAND_MAX);

    for (auto &i : pointIndices.indices) {
      PointRGB point;
      pcl::copyPoint(cloud->points[i], point);
      point.r = r;
      point.g = g;
      point.b = b;
      cloudRGB->push_back(point);
    }
  }
}


template <typename PointT>
void
MapClustering<PointT>::visualizePointsOutsideClusters(const CloudPtr &cloud,
                                                      const pcl::IndicesClustersPtr &clusters,
                                                      CloudRGBPtr &cloudRGB) {

  // Create vector with all pointcloud indices
  pcl::Indices indices;
  indices.reserve(cloud->size());
  for (int i = 0; i < cloud->size(); i++)
    indices.push_back(i);

  // Put cluster indices to the vector
  pcl::Indices clusterIndices;
  for (auto &pointIndices : *clusters) {
    clusterIndices.insert(clusterIndices.end(), pointIndices.indices.begin(), pointIndices.indices.end());
  }

  // Take difference of all indices and cluster indices
  pcl::Indices nonclusterIndices;
  std::sort (clusterIndices.begin(), clusterIndices.end());
  std::set_difference(indices.begin(), indices.end(),
                      clusterIndices.begin(), clusterIndices.end(),
                      std::inserter(nonclusterIndices, nonclusterIndices.begin()));

  // Save the outliers to the cloud as grey points into the cloud
  PointRGB point;
  for (auto idx : nonclusterIndices) {
    //std::cout << cloud->points.size() << " " << idx << std::endl;
    pcl::copyPoint(cloud->points[idx], point);
    point.r = point.g = point.b = 128;
    cloudRGB->push_back(point);
  }
}


template <typename PointT>
std::vector<pcl::octree::OctreeCovarianceContainer<PointT>*>
MapClustering<PointT>::getLeaves() {
  std::vector<pcl::octree::OctreeCovarianceContainer<PointT>*> leavesSegment;
  octreeEaNdt_->serializeLeafs(leavesSegment);

  return leavesSegment;
}


template <typename PointT>
void
MapClustering<PointT>::fillLabel(CloudPtr &cloud, pcl::IndicesClustersPtr &indicesClusters) {
  // Initialize all the labels to maximum value -> means unlabeled
  for (auto &point : *cloud)
    point.inst_label = std::numeric_limits<uint16_t>::max();

  for (int label = 0; label < indicesClusters->size(); label++) {
    for (auto idx : indicesClusters->at(label).indices)
      cloud->at(idx).inst_label = label;
  }
}


template <typename PointT>
void
MapClustering<PointT>::rosPublisher(ros::NodeHandle &node,
                                    bool segments,
                                    bool instances,
                                    bool primitives,
                                    bool cells) {

  // Declare publishers
  ros::Publisher ea_ndt_pub = node.advertise<visualization_msgs::MarkerArray> ( "ea_ndt_cells", 1 );
  ros::Publisher ndt_pub = node.advertise<visualization_msgs::MarkerArray> ( "ndt_cells", 1 );
  ros::Publisher ndt_grid_pub = node.advertise<visualization_msgs::Marker> ( "ndt_grid", 1 );
  ros::Publisher cloud_pub = node.advertise<CloudI> ("cloud", 1);
  ros::Publisher cloud_pub_segments = node.advertise<CloudRGB> ("cloud_segments", 1);
  ros::Publisher cloud_pub_instances = node.advertise<CloudRGB> ("cloud_instances", 1);
  ros::Publisher cloud_pub_primitives = node.advertise<CloudRGB> ("cloud_primitives", 1);
  ros::Publisher cloud_pub_cells = node.advertise<CloudRGB> ("cloud_cells", 1);

  std::cout << "\nROS visualization\n";

  // Make published objects
  pcl::console::TicToc tt;
  tt.tic ();
  std::cout << "    " << std::setw(49) << std::left << "Preparing the markerArrays for EA-NDT and NDT..." << std::flush;
  visualization_msgs::MarkerArray markerArraySpheresEaNdtCells;
  visualization_msgs::MarkerArray markerArraySpheresNdtCells;
  visualization_msgs::Marker markerNdtGrid;
  getVisualizedEaNdtCells(markerArraySpheresEaNdtCells);
  getVisualizedNdtCells(markerArraySpheresNdtCells, markerNdtGrid);
  std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3 << " sec\n";

  CloudIPtr cloudIntensity (new CloudI);
  CloudRGBPtr cloudSegments (new CloudRGB),
              cloudInstances (new CloudRGB),
              cloudPrimitives (new CloudRGB),
              cloudCells (new CloudRGB);

  // Set frame id
  cloud_->header.frame_id = "map";
  cloud_->width = cloud_->size();
  cloud_->height = 1;
  cloudIntensity->header.frame_id = "map";
  cloudSegments->header.frame_id = "map";
  cloudInstances->header.frame_id = "map";
  cloudPrimitives->header.frame_id = "map";
  cloudCells->header.frame_id = "map";

  // Create clouds to be published
  pcl::copyPointCloud(*cloud_, *cloudIntensity);

  // Define an octree
  pcl::octree::OctreePointCloudVoxelCentroid<PointI> octree (0.05f);
  octree.setInputCloud(cloudIntensity);
  octree.addPointsFromInputCloud();

  // Extract octree filtered points and create a point cloud from them
  pcl::octree::OctreePointCloudVoxelCentroid<PointI>::AlignedPointTVector centroids;
  octree.getVoxelCentroids(centroids);

  cloudIntensity->clear();
  cloudIntensity->points.assign (centroids.begin(), centroids.end());
  cloudIntensity->width = uint32_t (centroids.size());
  cloudIntensity->height = 1;
  cloudIntensity->is_dense = false;

  if (segments) {
    tt.tic ();
    std::cout << "    " << std::setw(49) << std::left << "Preparing RGB cloud of segments..." << std::flush;
    pcl_conversions::toPCL(ros::Time::now(), cloudSegments->header.stamp);
    makeRGBIntensityCloud(*cloud_, *cloudSegments);

    if (!cloudSegments->size()) {
      std::cout << "Semantic segments not available, cannot be visualized." << std::endl;
      segments = false;
    }
    else {
      // Define an octree
      pcl::octree::OctreePointCloudVoxelCentroid<PointRGB> octree (0.05f);
      octree.setInputCloud(cloudSegments);
      octree.addPointsFromInputCloud();

      // Extract octree filtered points and create a point cloud from them
      pcl::octree::OctreePointCloudVoxelCentroid<PointRGB>::AlignedPointTVector centroids;
      octree.getVoxelCentroids(centroids);

      cloudSegments->clear();
      cloudSegments->points.assign (centroids.begin(), centroids.end());
      cloudSegments->width = uint32_t (centroids.size());
      cloudSegments->height = 1;
      cloudSegments->is_dense = false;

      std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3 << " sec\n";
    }
  }
  if (instances) {
    tt.tic ();
    std::cout << "    " << std::setw(49) << std::left << "Preparing RGB cloud of instances..." << std::flush;
    pcl_conversions::toPCL(ros::Time::now(), cloudInstances->header.stamp);
    getVisualizedInstances(cloudInstances);
    if (!cloudInstances->size()) {
      std::cout << "Instance clusters not available, cannot be visualized." << std::endl;
      instances = false;
    }
    else {
      // Define an octree
      pcl::octree::OctreePointCloudVoxelCentroid<PointRGB> octree (0.05f);
      octree.setInputCloud(cloudInstances);
      octree.addPointsFromInputCloud();

      // Extract octree filtered points and create a point cloud from them
      pcl::octree::OctreePointCloudVoxelCentroid<PointRGB>::AlignedPointTVector centroids;
      octree.getVoxelCentroids(centroids);

      cloudInstances->clear();
      cloudInstances->points.assign (centroids.begin(), centroids.end());
      cloudInstances->width = uint32_t (centroids.size());
      cloudInstances->height = 1;
      cloudInstances->is_dense = false;

      std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3 << " sec\n";
    }
  }
  if (primitives) {
    tt.tic ();
    std::cout << "    " << std::setw(49) << std::left << "Preparing RGB cloud of primitives..." << std::flush;
    pcl_conversions::toPCL(ros::Time::now(), cloudPrimitives->header.stamp);
    getVisualizedPrimitives(cloudPrimitives);
    if (!cloudPrimitives->size()) {
      std::cout << "Extracted primitives not available, cannot be visualized." << std::endl;
      primitives = false;
    }
    else {
      // Define an octree
      pcl::octree::OctreePointCloudVoxelCentroid<PointRGB> octree (0.05f);
      octree.setInputCloud(cloudPrimitives);
      octree.addPointsFromInputCloud();

      // Extract octree filtered points and create a point cloud from them
      pcl::octree::OctreePointCloudVoxelCentroid<PointRGB>::AlignedPointTVector centroids;
      octree.getVoxelCentroids(centroids);

      cloudPrimitives->clear();
      cloudPrimitives->points.assign (centroids.begin(), centroids.end());
      cloudPrimitives->width = uint32_t (centroids.size());
      cloudPrimitives->height = 1;
      cloudPrimitives->is_dense = false;

      std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3 << " sec\n";
    }
  }
  if (cells) {
    tt.tic ();
    std::cout << "    " << std::setw(49) << std::left << "Preparing RGB cloud of cells..." << std::flush;
    pcl_conversions::toPCL(ros::Time::now(), cloudCells->header.stamp);
    getVisualizedCells(cloudCells);
    if (!cloudCells->size()) {
      std::cout << "Cell clusters not available, cannot be visualized." << std::endl;
      cells = false;
    }
    else {
      // Define an octree
      pcl::octree::OctreePointCloudVoxelCentroid<PointRGB> octree (0.05f);
      octree.setInputCloud(cloudCells);
      octree.addPointsFromInputCloud();

      // Extract octree filtered points and create a point cloud from them
      pcl::octree::OctreePointCloudVoxelCentroid<PointRGB>::AlignedPointTVector centroids;
      octree.getVoxelCentroids(centroids);

      cloudCells->clear();
      cloudCells->points.assign (centroids.begin(), centroids.end());
      cloudCells->width = uint32_t (centroids.size());
      cloudCells->height = 1;
      cloudCells->is_dense = false;

      std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3 << " sec\n";
    }
  }

  // Publish msgs
  std::cout << "\n    Visualizing, press any key to stop publishing in ROS and continue computation..." << std::endl;
  while (!kbhit()) {
    ea_ndt_pub.publish (markerArraySpheresEaNdtCells);
    ndt_pub.publish (markerArraySpheresNdtCells);
    ndt_grid_pub.publish (markerNdtGrid);

    cloud_pub.publish(cloudIntensity);
    if (segments)
      cloud_pub_segments.publish (cloudSegments);
    if (instances)
      cloud_pub_instances.publish (cloudInstances);
    if (primitives)
      cloud_pub_primitives.publish (cloudPrimitives);
    if (cells)
      cloud_pub_cells.publish (cloudCells);

    usleep(1000000);
  }

}


template <typename PointT>
void
MapClustering<PointT>::getVisualizedEaNdtCells(visualization_msgs::MarkerArray &markerArraySpheres) const {
  getVisualizedEaNdtCells(octreeEaNdtGround_,   markerArraySpheres, {255, 127, 255});
  getVisualizedEaNdtCells(octreeEaNdtBuilding_, markerArraySpheres, {255, 255,   0});
  getVisualizedEaNdtCells(octreeEaNdtFence_,    markerArraySpheres, {  0, 255, 255});
  getVisualizedEaNdtCells(octreeEaNdtTrunk_,    markerArraySpheres, {255, 127,   0});
  getVisualizedEaNdtCells(octreeEaNdtPole_,     markerArraySpheres, {  0,   0, 255});
  getVisualizedEaNdtCells(octreeEaNdtSign_,     markerArraySpheres, {255,   0,   0});
}

// Get Gaussian spheres
template <typename PointT>
void
MapClustering<PointT>::getVisualizedEaNdtCells(const OctreeSegmentTPtr &octree,
                                               visualization_msgs::MarkerArray &markerArraySpheres,
                                               const std::vector<int> &RGB_spheres) const {

  int i = markerArraySpheres.markers.size();
  for (auto leaf_it = octree->leaf_depth_begin(); leaf_it != octree->leaf_depth_end(); ++leaf_it) {
    auto &leafContainer = leaf_it.getLeafContainer();
    if (leafContainer.getPointCount() < 6)
      continue;

    Eigen::Vector3d mean = leafContainer.getMean ();
    Eigen::Vector3d eigenValues = leafContainer.getEvals ();
    Eigen::Matrix3d eigenVectors = leafContainer.getEvecs ();

    if ( (eigenValues(0) == 0.) || (eigenValues(1) == 0.) || (eigenValues(2) == 0.) )
      continue;

    // Create sphere for gaussian distribution
    visualization_msgs::Marker marker_sphere;
    marker_sphere.header.frame_id = "map";
    marker_sphere.header.stamp = ros::Time::now();
    marker_sphere.type = visualization_msgs::Marker::SPHERE;
    marker_sphere.action = visualization_msgs::Marker::ADD;
    marker_sphere.id = i++;
    marker_sphere.pose.position.x = mean(0);
    marker_sphere.pose.position.y = mean(1);
    marker_sphere.pose.position.z = mean(2);

    // Flip Z axis of eigen vector matrix if determinant is -1
    Eigen::Quaterniond q;
    if (eigenVectors.determinant() < 0) {
      Eigen::Matrix3d flipZ180 = Eigen::Matrix3d::Identity();
      flipZ180(2,2) = -1;
      q = eigenVectors * flipZ180;
    }
    else
      q = eigenVectors;

    q.normalize();
    marker_sphere.pose.orientation.x = q.x();
    marker_sphere.pose.orientation.y = q.y();
    marker_sphere.pose.orientation.z = q.z();
    marker_sphere.pose.orientation.w = q.w();
    marker_sphere.scale.x = sqrt(eigenValues(0)) * 2.f; // 2 sigma ~95,4%
    marker_sphere.scale.y = sqrt(eigenValues(1)) * 2.f; // 2 sigma ~95,4%
    marker_sphere.scale.z = sqrt(eigenValues(2)) * 2.f; // 2 sigma ~95,4%
    marker_sphere.color.a = 1.0;
    marker_sphere.color.r = RGB_spheres[0] / 255.f;
    marker_sphere.color.g = RGB_spheres[1] / 255.f;
    marker_sphere.color.b = RGB_spheres[2] / 255.f;

    markerArraySpheres.markers.push_back(marker_sphere);
  }
}


// Get Gaussian spheres
template <typename PointT>
void
MapClustering<PointT>::getVisualizedNdtCells(visualization_msgs::MarkerArray &markerArraySpheres,
                                             visualization_msgs::Marker &markerLineList,
                                             const std::vector<int> &RGB_spheres,
                                             const std::vector<int> &RGB_lines) const {

  if (octreeNdt_->getLeafCount() < 1)
    return;

  // Prefill the common part for all lines
  markerLineList.header.frame_id = "map";
  markerLineList.header.stamp = ros::Time::now();
  markerLineList.ns = "grid";
  markerLineList.type = visualization_msgs::Marker::LINE_LIST;
  markerLineList.action = visualization_msgs::Marker::ADD;
  markerLineList.id = 0;
  markerLineList.scale.x = 2e-2;
  markerLineList.scale.y = 2e-2;
  markerLineList.scale.z = 2e-2;
  markerLineList.pose.orientation.x = 0.f;
  markerLineList.pose.orientation.y = 0.f;
  markerLineList.pose.orientation.z = 0.f;
  markerLineList.pose.orientation.w = 1.f;
  markerLineList.color.a = 0.5;
  markerLineList.color.r = RGB_lines[0] / 255.f;
  markerLineList.color.g = RGB_lines[1] / 255.f;
  markerLineList.color.b = RGB_lines[2] / 255.f;

  int i = markerArraySpheres.markers.size();
  for (auto leaf_it = octreeNdt_->leaf_depth_begin(); leaf_it != octreeNdt_->leaf_depth_end(); ++leaf_it) {
    auto &leafContainer = leaf_it.getLeafContainer();
    if (leafContainer.getPointCount() < 6)
      continue;

    Eigen::Vector3d mean = leafContainer.getMean ();
    Eigen::Vector3d eigenValues = leafContainer.getEvals ();
    Eigen::Matrix3d eigenVectors = leafContainer.getEvecs ();

    if ( (eigenValues(0) == 0.) || (eigenValues(1) == 0.) || (eigenValues(2) == 0.) )
      continue;

    // Create sphere for gaussian distribution
    visualization_msgs::Marker marker_sphere;
    marker_sphere.header.frame_id = "map";
    marker_sphere.header.stamp = ros::Time::now();
    marker_sphere.ns = "grid";
    marker_sphere.type = visualization_msgs::Marker::SPHERE;
    marker_sphere.action = visualization_msgs::Marker::ADD;
    marker_sphere.id = i++;
    marker_sphere.pose.position.x = mean(0);
    marker_sphere.pose.position.y = mean(1);
    marker_sphere.pose.position.z = mean(2);

    // Flip Z axis of eigen vector matrix if determinant is -1
    Eigen::Quaterniond q;
    if (eigenVectors.determinant() < 0) {
      Eigen::Matrix3d flipZ180 = Eigen::Matrix3d::Identity();
      flipZ180(2,2) = -1;
      q = eigenVectors * flipZ180;
    }
    else
      q = eigenVectors;

    q.normalize();
    marker_sphere.pose.orientation.x = q.x();
    marker_sphere.pose.orientation.y = q.y();
    marker_sphere.pose.orientation.z = q.z();
    marker_sphere.pose.orientation.w = q.w();
    marker_sphere.scale.x = sqrt(eigenValues(0)) * 2.f; // 2 sigma ~95,4%
    marker_sphere.scale.y = sqrt(eigenValues(1)) * 2.f; // 2 sigma ~95,4%
    marker_sphere.scale.z = sqrt(eigenValues(2)) * 2.f; // 2 sigma ~95,4%
    marker_sphere.color.a = 1.0;
    marker_sphere.color.r = RGB_spheres[0] / 255.f;
    marker_sphere.color.g = RGB_spheres[1] / 255.f;
    marker_sphere.color.b = RGB_spheres[2] / 255.f;

    markerArraySpheres.markers.push_back(marker_sphere);

    Eigen::Vector3f min_pt, max_pt;
    octreeNdt_->getVoxelBounds(leaf_it, min_pt, max_pt);

    geometry_msgs::Point point;
    point.x = min_pt[0];
    point.y = min_pt[1];
    point.z = min_pt[2];

    markerLineList.points.push_back(point);
    point.x += cellSize_;
    markerLineList.points.push_back(point);
    markerLineList.points.push_back(point);
    point.z += cellSize_;
    markerLineList.points.push_back(point);
    markerLineList.points.push_back(point);
    point.x -= cellSize_;
    markerLineList.points.push_back(point);
    markerLineList.points.push_back(point);
    point.z -= cellSize_;
    markerLineList.points.push_back(point);
    markerLineList.points.push_back(point);
    point.y += cellSize_;
    markerLineList.points.push_back(point);
    markerLineList.points.push_back(point);
    point.x += cellSize_;
    markerLineList.points.push_back(point);
    markerLineList.points.push_back(point);
    point.z += cellSize_;
    markerLineList.points.push_back(point);
    markerLineList.points.push_back(point);
    point.x -= cellSize_;
    markerLineList.points.push_back(point);
    markerLineList.points.push_back(point);
    point.z -= cellSize_;
    markerLineList.points.push_back(point);
    point.z += cellSize_;
    markerLineList.points.push_back(point);
    point.y -= cellSize_;
    markerLineList.points.push_back(point);
    point.y += cellSize_;
    point.x += cellSize_;
    markerLineList.points.push_back(point);
    point.y -= cellSize_;
    markerLineList.points.push_back(point);
    point.y += cellSize_;
    point.z -= cellSize_;
    markerLineList.points.push_back(point);
    point.y -= cellSize_;
    markerLineList.points.push_back(point);
  }
}

#endif /* SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_MAPCLUSTERING_HPP_ */
