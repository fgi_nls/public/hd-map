/*
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SOURCE_DIRECTORY__INSTANCE_SEGMENTATION_SRC_PLANESEGMENTATION_HPP_
#define SOURCE_DIRECTORY__INSTANCE_SEGMENTATION_SRC_PLANESEGMENTATION_HPP_

#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

#include <pcl/impl/instantiate.hpp>
#include <pcl/segmentation/impl/sac_segmentation.hpp>
#include <pcl/sample_consensus/impl/sac_model_plane.hpp>
#include <pcl/sample_consensus/impl/sac_model_line.hpp>
#include <pcl/sample_consensus/impl/sac_model_stick.hpp>
#include <pcl/sample_consensus/impl/sac_model_parallel_plane.hpp>
#include <pcl/sample_consensus/impl/sac_model_cylinder.hpp>
#include <pcl/sample_consensus/impl/sac_model_circle.hpp>
#include <pcl/sample_consensus/impl/sac_model_circle3d.hpp>
#include <pcl/sample_consensus/impl/sac_model_sphere.hpp>
#include <pcl/sample_consensus/impl/sac_model_parallel_line.hpp>
#include <pcl/sample_consensus/impl/sac_model_perpendicular_plane.hpp>
#include <pcl/sample_consensus/impl/sac_model_parallel_plane.hpp>
#include <pcl/sample_consensus/impl/sac_model_normal_plane.hpp>
#include <pcl/sample_consensus/impl/sac_model_normal_parallel_plane.hpp>
#include <pcl/sample_consensus/impl/sac_model_cone.hpp>
#include <pcl/sample_consensus/impl/sac_model_normal_sphere.hpp>
#include <pcl/sample_consensus/impl/ransac.hpp>
#include <pcl/sample_consensus/impl/lmeds.hpp>
#include <pcl/sample_consensus/impl/msac.hpp>
#include <pcl/sample_consensus/impl/rransac.hpp>
#include <pcl/sample_consensus/impl/rmsac.hpp>
#include <pcl/sample_consensus/impl/mlesac.hpp>
#include <pcl/sample_consensus/impl/prosac.hpp>
#include <pcl/impl/pcl_base.hpp>

#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

#include <pcl/console/time.h>
#include <pcl/features/normal_3d.h>
#include <pcl/segmentation/conditional_euclidean_clustering.h>

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/features/integral_image_normal.h>

#include <pcl/filters/filter_indices.h>
#include <pcl/common/transforms.h>
#include <pcl/common/centroid.h>
#include <pcl/common/common.h>

#include <algorithm>

#include <pcl/octree/octree_pointcloud_voxelcentroid.h>
#include <pcl/octree/octree_pointcloud_pointvector.h>

template class pcl::SACSegmentation<PointTD2L>;
template class pcl::SACSegmentationFromNormals<PointTD2L, pcl::PointXYZINormal>;


bool
enforceCurvatureSimilarity (const pcl::PointXYZINormal &point_a,
                            const pcl::PointXYZINormal &point_b,
                            float squared_distance)
{
  Eigen::Map<const Eigen::Vector3f> point_a_normal = point_a.getNormalVector3fMap (),
                                    point_b_normal = point_b.getNormalVector3fMap ();
  if (std::abs (point_a_normal.dot (point_b_normal)) > 0.99)
    return true;

  return false;
}


template <typename PointT>
class PlaneSegmentation {
public:
  typedef typename pcl::PointCloud<PointT> Cloud;
  typedef typename pcl::PointCloud<PointT>::Ptr CloudPtr;
  typedef typename pcl::PointCloud<PointT>::ConstPtr CloudConstPtr;

  typedef typename pcl::octree::OctreePointCloud<PointT>::IndicesPtr OctreeIndicesPtr;
  typedef typename pcl::octree::OctreePointCloudVoxelCentroid<PointT> Octree;
  typedef typename Octree::AlignedPointTVector AlignedPointTVector;

  PlaneSegmentation();
  virtual ~PlaneSegmentation();

  void process(CloudPtr cloud, pcl::IndicesClustersPtr planeClusters);
  void segmentGroundPlane(CloudPtr cloud, pcl::IndicesClustersPtr groundClusters, float distanceThreshold);
  void findGroundPatchPlanes(CloudPtr cloud,
                             pcl::IndicesClustersPtr groundClusters,
                             std::vector<pcl::ModelCoefficientsPtr> &modelCoeffVec);

private:
  void segmentPlane(pcl::PointIndicesPtr &indices, pcl::IndicesClustersPtr &planeClusters);

  pcl::search::KdTree<pcl::PointXYZ>::Ptr searchTree_;
  pcl::NormalEstimation<pcl::PointXYZ, pcl::PointXYZINormal> ne_;
  pcl::SACSegmentationFromNormals<pcl::PointXYZ, pcl::PointXYZINormal> segmenter_;
  pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec_;
};


template <typename PointT>
PlaneSegmentation<PointT>::PlaneSegmentation()
: searchTree_ (new pcl::search::KdTree<pcl::PointXYZ>)
  {
  ne_.setSearchMethod (searchTree_);
  ne_.setKSearch (26);

  segmenter_.setOptimizeCoefficients (true); //Optional
  segmenter_.setModelType (pcl::SACMODEL_NORMAL_PLANE);
  segmenter_.setMethodType (pcl::SAC_RANSAC);
  segmenter_.setNumberOfThreads (0);
  segmenter_.setDistanceThreshold (0.15);
  segmenter_.setNormalDistanceWeight(0.25); // 0.25*PI ≃ 22.5 degree -> 67.5 degree difference allowed

  ec_.setClusterTolerance (0.3); // 30cm
  ec_.setMinClusterSize (50);

  // This turns off the errors when plane segmentation cannot fit a model or there are not enough points left
  pcl::console::setVerbosityLevel(pcl::console::L_ALWAYS);
}


template <typename PointT>
PlaneSegmentation<PointT>::~PlaneSegmentation() {
  // TODO Auto-generated destructor stub
}


template <typename PointT>
void PlaneSegmentation<PointT>::process(CloudPtr cloud, pcl::IndicesClustersPtr planeClusters) {

  float resolution = 0.1f;

  //pcl::PointCloud<pcl::PointXYZINormal>::Ptr  cloudNormal (new pcl::PointCloud<pcl::PointXYZINormal>);

  //pcl::copyPointCloud (*cloud, *cloudXYZ);
  //pcl::copyPointCloud (*cloud, *cloudNormal);

  pcl::IndicesClustersPtr instanceClusters (new pcl::IndicesClusters);
  for (int index = 0; index < cloud->size(); index++) {
    int inst_label = cloud->points[index].inst_label;
    if (instanceClusters->size() <= inst_label)
      instanceClusters->resize(inst_label + 1);
    instanceClusters->at(inst_label).indices.push_back(index);
  }

  //CloudPtr cloudOctree (new Cloud);

  for (auto &instance : *instanceClusters){
    if (instance.indices.size() < 3) // A plane cannot be fitted for less than 3 points
      continue;

    // Filtering data with octree to speed up the process
    OctreeIndicesPtr octreeIndices (new std::vector<int>);
    *octreeIndices = instance.indices;

    Octree octreeCentroids (resolution);
    octreeCentroids.setInputCloud(cloud, octreeIndices);
    octreeCentroids.addPointsFromInputCloud();

    AlignedPointTVector centroids;
    octreeCentroids.getVoxelCentroids(centroids);

    CloudPtr cloudSparse (new Cloud);
    cloudSparse->points.assign (centroids.begin(), centroids.end());
    cloudSparse->width = uint32_t (centroids.size());
    cloudSparse->height = 1;
    cloudSparse->is_dense = false;

    if (cloudSparse->size() < 3) // A plane cannot be fitted for less than 3 points
      continue;

    // Create another octree to save the points for each cell
    pcl::octree::OctreePointCloudPointVector<PointT> octreeVectors (resolution);
    octreeVectors.setInputCloud(cloud, octreeIndices);
    octreeVectors.addPointsFromInputCloud();
    double min_x_arg, min_y_arg, min_z_arg, max_x_arg, max_y_arg, max_z_arg;
    octreeVectors.getBoundingBox (min_x_arg, min_y_arg, min_z_arg, max_x_arg, max_y_arg, max_z_arg);

    /*if (cloudSparse->size() < 10) {
      std::cout << "cloudSparse->size(): " << cloudSparse->size() << std::endl;
      std::cout << "octreeIndices->size(): " << octreeIndices->size() << std::endl;

      std::cout << "octreeIndices: ";
      for (auto &idx : *octreeIndices)
        std::cout << idx << " ";

      std::cout << std::endl << "octreePointVector: ";
      pcl::Indices octreeVectorIndices;
      for (auto &point : cloudSparse->points) {
        unsigned int x = static_cast<unsigned int>((point.x - min_x_arg) / resolution);
        unsigned int y = static_cast<unsigned int>((point.y - min_y_arg) / resolution);
        unsigned int z = static_cast<unsigned int>((point.z - min_z_arg) / resolution);

        auto leafContainer = octreeVectors.findLeaf(x, y, z);
        pcl::Indices indices;
        leafContainer->getPointIndices(indices);
        octreeVectorIndices.insert(octreeVectorIndices.end(), indices.begin(), indices.end());
      }

      std::sort(octreeVectorIndices.begin(), octreeVectorIndices.end());
      for (auto &idx : octreeVectorIndices)
        std::cout << idx << " ";
      std::cout << std::endl << std::endl;
    }*/

    // Demean instance and compute normals
    Eigen::Vector4f centroid;
    pcl::compute3DCentroid(*cloudSparse, centroid);

    pcl::PointCloud<pcl::PointXYZ>::Ptr  cloudDemeaned (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::copyPointCloud (*cloudSparse, *cloudDemeaned);
    pcl::demeanPointCloud(*cloudDemeaned, centroid, *cloudDemeaned);

    pcl::PointCloud<pcl::PointXYZINormal>::Ptr  cloudDemeanedNormals (new pcl::PointCloud<pcl::PointXYZINormal>);
    pcl::copyPointCloud (*cloudDemeaned, *cloudDemeanedNormals);
    ne_.setInputCloud (cloudDemeaned);
    ne_.compute (*cloudDemeanedNormals);

    // Create temporal indices vector to search planes in the while loop
    pcl::PointIndicesPtr indicesDemeaned (new pcl::PointIndices);
    for (int i = 0; i < cloudDemeaned->points.size(); i++)
      indicesDemeaned->indices.push_back(i);

    // Provide inputs for RANSAC segmentation object
    segmenter_.setInputCloud (cloudDemeaned);
    segmenter_.setInputNormals (cloudDemeanedNormals);

    // Create KD-Tree for euclidean clustering
    pcl::search::KdTree<Point>::Ptr tree (new pcl::search::KdTree<Point>);
    tree->setInputCloud (cloudDemeaned);

    // Initialize euclidean clustering, will be used in segmentPlane methods
    ec_.setSearchMethod (tree);
    ec_.setInputCloud (cloudDemeaned);

    pcl::IndicesClustersPtr instancePlaneClusters (new pcl::IndicesClusters);
    segmentPlane(indicesDemeaned, instancePlaneClusters);

    // Associate inliers into sparse cloud and save
    /*for (auto &planeIndices : *instancePlaneClusters) {
      for (auto &planeIndex : planeIndices.indices) {
        std::cout << "planeIdx: " << planeIndex << " ";
        planeIndex = indicesDemeaned->indices [planeIndex];
        std::cout << planeIndex << std::endl;
      }
    }*/

    // Associate inliers into original dense cloud and save
    //int it_count = 1;
    pcl::IndicesClustersPtr denseCloudPlaneClusters (new pcl::IndicesClusters);
    for (auto &planeIndices : *instancePlaneClusters) {
      pcl::PointIndices denseCloudPlaneIndices;
      //std::sort(planeIndices.indices.begin(), planeIndices.indices.end());
      for (auto &planeIndex : planeIndices.indices) {
        PointT point = cloudSparse->points[planeIndex];
        unsigned int x = static_cast<unsigned int>((point.x - min_x_arg) / resolution);
        unsigned int y = static_cast<unsigned int>((point.y - min_y_arg) / resolution);
        unsigned int z = static_cast<unsigned int>((point.z - min_z_arg) / resolution);

        auto leafContainer = octreeVectors.findLeaf(x, y, z);
        pcl::Indices indices;
        leafContainer->getPointIndices(indices);
        denseCloudPlaneIndices.indices.insert(denseCloudPlaneIndices.indices.end(), indices.begin(), indices.end());

        //cloudSparse->points[planeIndex].inst_label = 65535 * it_count / instancePlaneClusters->size();
      }
      //it_count++;
      //std::cout << denseCloudPlaneIndices.indices.size() << std::endl;
      denseCloudPlaneClusters->push_back(denseCloudPlaneIndices);
    }

    //*cloudOctree += *cloudSparse;



    planeClusters->insert( planeClusters->end(), denseCloudPlaneClusters->begin(), denseCloudPlaneClusters->end() );
  }

  //pcl::io::savePCDFile<PointT> ("/home/petri/Data/RandLA-Net_data/VLS128_pointclouds/Datankeruu_kaupunkipuu/2020-09-07/"
  //        "workspace_kaupunkipuu_VLS128_pointclouds_2020-09-07-13-52-45/filtered_instances_1cm_voxel/mini_test_patch/"
  //        "test_plane_segmentation.pcd", *cloudOctree);
}


template <typename PointT>
void
PlaneSegmentation<PointT>::segmentPlane(pcl::PointIndicesPtr &indices, pcl::IndicesClustersPtr &planeClusters) {

  // Set indices and segment a plane
  pcl::ModelCoefficientsPtr coefficients (new pcl::ModelCoefficients);
  pcl::PointIndicesPtr planeInliers (new pcl::PointIndices);
  segmenter_.setIndices (indices);
  segmenter_.segment (*planeInliers, *coefficients);

  if (!planeInliers->indices.size())
    return;

  // In case that the plane is formed of several clusters, divide the found plane into clusters
  pcl::IndicesClustersPtr euclideanClusters (new pcl::IndicesClusters);
  ec_.setIndices (planeInliers);
  ec_.extract (*euclideanClusters);

  // Save found plane clusters
  for (auto &euclideanCluster : *euclideanClusters) {
    pcl::PointIndicesPtr euclideanClusterPtr (new pcl::PointIndices);
    *euclideanClusterPtr = euclideanCluster;
    planeClusters->push_back(*euclideanClusterPtr);
  }

  // Remove plane inliers from the original set of indices
  pcl::Indices remainingIndices;
  std::set_difference(indices->indices.begin(), indices->indices.end(),
                      planeInliers->indices.begin(), planeInliers->indices.end(),
                      std::inserter(remainingIndices, remainingIndices.begin()));

  indices->indices = remainingIndices;

  // Euclidean clustering to divide the remaining points into clusters
  euclideanClusters->clear();
  ec_.setIndices (indices);
  ec_.extract (*euclideanClusters);

  for (auto &euclideanCluster : *euclideanClusters) {
    if (euclideanCluster.indices.size() > ec_.getMinClusterSize()) {
      pcl::PointIndicesPtr euclideanClusterPtr (new pcl::PointIndices);
      *euclideanClusterPtr = euclideanCluster;
      segmentPlane(euclideanClusterPtr, planeClusters);
    }
  }
}


template <typename PointT>
void
PlaneSegmentation<PointT>::segmentGroundPlane(CloudPtr cloud, pcl::IndicesClustersPtr groundClusters, float distanceThreshold) {
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloudXYZ (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr cloudNormal (new pcl::PointCloud<pcl::PointXYZINormal>);

  pcl::copyPointCloud (*cloud, *cloudXYZ);
  pcl::copyPointCloud (*cloud, *cloudNormal);

  pcl::SACSegmentationFromNormals<pcl::PointXYZ, pcl::PointXYZINormal> segmenter;
  segmenter.setOptimizeCoefficients (true); //Optional
  segmenter.setModelType (pcl::SACMODEL_NORMAL_PARALLEL_PLANE);
  segmenter.setMethodType (pcl::SAC_RANSAC);
  segmenter.setNumberOfThreads (0);
  segmenter.setDistanceThreshold (distanceThreshold);
  segmenter.setNormalDistanceWeight(0.25);
  segmenter.setAxis (Eigen::Vector3f (0., 0., 1.));
  segmenter.setEpsAngle ( 30.0f * (M_PI/180.0f) ); // 30 degrees of difference allowed with

  for (auto &groundClusterIndices : *groundClusters){
    if (groundClusterIndices.indices.size() < 4)
      continue;

    // Demean instance and compute normals
    Eigen::Vector4f centroid;
    pcl::compute3DCentroid(*cloud, groundClusterIndices, centroid);

    pcl::PointCloud<pcl::PointXYZ>::Ptr  cloudDemeaned (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::demeanPointCloud(*cloudXYZ, groundClusterIndices, centroid, *cloudDemeaned);
    ne_.setInputCloud (cloudDemeaned);

    pcl::PointCloud<pcl::PointXYZINormal>::Ptr  cloudDemeanedNormals (new pcl::PointCloud<pcl::PointXYZINormal>);
    pcl::copyPointCloud (*cloudDemeaned, *cloudDemeanedNormals);
    ne_.compute (*cloudDemeanedNormals);

    // Create temporal indices vector to search planes in the while loop
    pcl::PointIndicesPtr indicesDemeaned (new pcl::PointIndices);
    for (int i = 0; i < cloudDemeaned->points.size(); i++)
      indicesDemeaned->indices.push_back(i);

    // Provide inputs for RANSAC segmentation object
    pcl::ModelCoefficientsPtr coefficients (new pcl::ModelCoefficients);
    pcl::PointIndicesPtr planeInliers (new pcl::PointIndices);

    segmenter.setInputCloud (cloudDemeaned);
    segmenter.setInputNormals (cloudDemeanedNormals);
    segmenter.setIndices (indicesDemeaned);
    segmenter.segment (*planeInliers, *coefficients);

    if (planeInliers->indices.size() < 1)
      continue;

    for (auto &index : planeInliers->indices)
      index = groundClusterIndices.indices[index];

    groundClusterIndices = *planeInliers;
  }
}


template <typename PointT>
void
PlaneSegmentation<PointT>::findGroundPatchPlanes(CloudPtr cloud,
                                                 pcl::IndicesClustersPtr groundClusters,
                                                 std::vector<pcl::ModelCoefficientsPtr> &modelCoeffVec) {

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloudXYZ (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZINormal>::Ptr cloudNormal (new pcl::PointCloud<pcl::PointXYZINormal>);

  pcl::copyPointCloud (*cloud, *cloudXYZ);
  pcl::copyPointCloud (*cloud, *cloudNormal);

  pcl::SACSegmentationFromNormals<pcl::PointXYZ, pcl::PointXYZINormal> segmenter;
  segmenter.setOptimizeCoefficients (true); // Optional
  segmenter.setModelType (pcl::SACMODEL_NORMAL_PARALLEL_PLANE);
  segmenter.setMethodType (pcl::SAC_RANSAC);
  segmenter.setNumberOfThreads (0);
  segmenter.setDistanceThreshold (0.25);
  segmenter.setNormalDistanceWeight(0.25);
  segmenter.setAxis (Eigen::Vector3f (0., 0., 1.));
  segmenter.setEpsAngle ( 30.0f * (M_PI/180.0f) ); // 30 degrees of difference allowed with

  for (auto &groundClusterIndices : *groundClusters){

    // Demean instance and compute normals
    Eigen::Vector4f centroid;
    pcl::compute3DCentroid(*cloud, groundClusterIndices, centroid);

    pcl::PointCloud<pcl::PointXYZ>::Ptr  cloudDemeaned (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::demeanPointCloud(*cloudXYZ, groundClusterIndices, centroid, *cloudDemeaned);
    ne_.setInputCloud (cloudDemeaned);

    pcl::PointCloud<pcl::PointXYZINormal>::Ptr  cloudDemeanedNormals (new pcl::PointCloud<pcl::PointXYZINormal>);
    pcl::copyPointCloud (*cloudDemeaned, *cloudDemeanedNormals);
    ne_.compute (*cloudDemeanedNormals);

    // Provide inputs for RANSAC segmentation object
    pcl::ModelCoefficientsPtr coefficients (new pcl::ModelCoefficients);
    pcl::PointIndicesPtr planeInliers (new pcl::PointIndices);
    segmenter.setInputCloud (cloudDemeaned);
    segmenter.setInputNormals (cloudDemeanedNormals);
    //segmenter.setIndices (indicesDemeaned);
    segmenter.segment (*planeInliers, *coefficients);

    modelCoeffVec.push_back(coefficients);
  }
}

#endif /* SOURCE_DIRECTORY__INSTANCE_SEGMENTATION_SRC_PLANESEGMENTATION_HPP_ */





