/*
 * Software License Agreement (BSD License)
 *
 *  Point Cloud Library (PCL) - www.pointclouds.org
 *  Copyright (c) 2010-2011, Willow Garage, Inc.
 *  Copyright (c) 2012-, Open Perception, Inc.
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREE_SEGMENT_COVARIANCE_HPP_
#define SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREE_SEGMENT_COVARIANCE_HPP_

/*
 * OctreePointCloudVoxelcontroid is not precompiled, since it's used in other
 * parts of PCL with custom LeafContainers. So if PCL_NO_PRECOMPILE is NOT
 * used, octree_pointcloud_voxelcentroid.h includes this file but octree_pointcloud.h
 * would not include the implementation because it's precompiled. So we need to
 * include it here "manually".
 */

#include <pcl/octree/impl/octree_search.hpp>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename PointT, typename LeafContainerT, typename BranchContainerT>
bool
pcl::octree::OctreeSegmentCovariance<PointT, LeafContainerT, BranchContainerT>::getVoxelCentroidAtPoint (
    const PointT& point_arg, PointT& voxel_centroid_arg) const
{
  OctreeKey key;
  LeafContainerT* leaf = NULL;

  // generate key
  genOctreeKeyforPoint (point_arg, key);

  leaf = this->findLeaf (key);
  if (leaf)
    leaf->getCentroid (voxel_centroid_arg);

  return (leaf != NULL);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename PointT, typename LeafContainerT, typename BranchContainerT>
void
pcl::octree::OctreeSegmentCovariance<PointT, LeafContainerT, BranchContainerT>::initContainerValuesRecursive (
    const BranchNode* branch_arg, OctreeKey& key_arg) const
{
  // iterate over all children
  for (unsigned char child_idx = 0; child_idx < 8; child_idx++)
  {
    // if child exist
    if (branch_arg->hasChild (child_idx))
    {
      // add current branch voxel to key
      key_arg.pushBranch (child_idx);

      OctreeNode *child_node = branch_arg->getChildPtr (child_idx);

      switch (child_node->getNodeType ())
      {
        case BRANCH_NODE:
        {
          // recursively proceed with indexed child branch
          initContainerValuesRecursive (static_cast<const BranchNode*> (child_node), key_arg);
          break;
        }
        case LEAF_NODE:
        {
          PointT new_centroid;

          LeafNode* container = static_cast<LeafNode*> (child_node);

          container->getContainer().initContainerValues(this->input_);

          break;
        }
        default:
          break;
       }

      // pop current branch voxel from key
      key_arg.popBranch ();
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template<typename PointT, typename LeafContainerT, typename BranchContainerT>
void
pcl::octree::OctreeSegmentCovariance<PointT, LeafContainerT, BranchContainerT>::getVoxelCentroidsRecursive (
    const BranchNode* branch_arg, OctreeKey& key_arg,
    typename OctreePointCloud<PointT, LeafContainerT, BranchContainerT>::AlignedPointTVector &voxel_centroid_list_arg) const
{
  // iterate over all children
  for (unsigned char child_idx = 0; child_idx < 8; child_idx++)
  {
    // if child exist
    if (branch_arg->hasChild (child_idx))
    {
      // add current branch voxel to key
      key_arg.pushBranch (child_idx);

      OctreeNode *child_node = branch_arg->getChildPtr (child_idx);

      switch (child_node->getNodeType ())
      {
        case BRANCH_NODE:
        {
          // recursively proceed with indexed child branch
          getVoxelCentroidsRecursive (static_cast<const BranchNode*> (child_node), key_arg, voxel_centroid_list_arg);
          break;
        }
        case LEAF_NODE:
        {
          PointT new_centroid;

          LeafNode* container = static_cast<LeafNode*> (child_node);

          container->getContainer().getCentroid (new_centroid);

          voxel_centroid_list_arg.push_back (new_centroid);
          break;
        }
        default:
          break;
       }

      // pop current branch voxel from key
      key_arg.popBranch ();
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointT, typename LeafContainerT, typename BranchContainerT>
double
pcl::octree::OctreeSegmentCovariance<PointT, LeafContainerT, BranchContainerT>::
getKNearestNeighborRecursive(const PointT& point,
                             std::size_t K,
                             const BranchNode* node,
                             const OctreeKey& key,
                             std::size_t tree_depth,
                             const double squared_search_radius,
                             std::vector<LeafContainerT> &leafs) const
{
  std::vector<prioBranchQueueEntry> search_heap;
  search_heap.resize(8);

  OctreeKey new_key;

  double smallest_squared_dist = squared_search_radius;

  // get spatial voxel information
  double voxelSquaredDiameter = this->getVoxelSquaredDiameter(tree_depth);

  // iterate over all children
  for (unsigned char child_idx = 0; child_idx < 8; child_idx++) {
    if (this->branchHasChild(*node, child_idx)) {
      PointT voxel_center;

      search_heap[child_idx].key.x = (key.x << 1) + (!!(child_idx & (1 << 2)));
      search_heap[child_idx].key.y = (key.y << 1) + (!!(child_idx & (1 << 1)));
      search_heap[child_idx].key.z = (key.z << 1) + (!!(child_idx & (1 << 0)));

      // generate voxel center point for voxel at key
      this->genVoxelCenterFromOctreeKey(
          search_heap[child_idx].key, tree_depth, voxel_center);

      // generate new priority queue element
      search_heap[child_idx].node = this->getBranchChildPtr(*node, child_idx);
      search_heap[child_idx].point_distance = this->pointSquaredDist(voxel_center, point);
    }
    else {
      search_heap[child_idx].point_distance = std::numeric_limits<float>::infinity();
    }
  }

  std::sort(search_heap.begin(), search_heap.end());

  // iterate over all children in priority queue
  // check if the distance to search candidate is smaller than the best point distance
  // (smallest_squared_dist)
  while ((!search_heap.empty()) &&
         (search_heap.back().point_distance <
          smallest_squared_dist + voxelSquaredDiameter / 4.0 +
              sqrt(smallest_squared_dist * voxelSquaredDiameter) - this->epsilon_)) {
    const OctreeNode* child_node;

    // read from priority queue element
    child_node = search_heap.back().node;
    new_key = search_heap.back().key;

    if (child_node->getNodeType() == BRANCH_NODE) {
      // we have not reached maximum tree depth
      smallest_squared_dist = getKNearestNeighborRecursive(point,
                                                           K,
                                                           static_cast<const BranchNode*>(child_node),
                                                           new_key,
                                                           tree_depth + 1,
                                                           smallest_squared_dist,
                                                           leafs);
    }
    else {
      // we reached leaf node level
      //Indices decoded_point_vector;

      const LeafNode* child_leaf = static_cast<const LeafNode*>(child_node);

      LeafContainerT leafContainer = child_leaf->getContainer();
      leafs.push_back(leafContainer);
    }
    // pop element from priority queue
    search_heap.pop_back();
  }

  return (smallest_squared_dist);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointT, typename LeafContainerT, typename BranchContainerT>
void
pcl::octree::OctreeSegmentCovariance<PointT, LeafContainerT, BranchContainerT>::
getNeighborsWithinRadiusRecursive(const PointT& point,
                                  const double radiusSquared,
                                  const BranchNode* node,
                                  const OctreeKey& key,
                                  std::size_t tree_depth,
                                  std::vector<LeafContainerT> &leafs,
                                  std::vector<float>& k_sqr_distances,
                                  std::size_t max_nn) const
{
  // get spatial voxel information
  double voxel_squared_diameter = this->getVoxelSquaredDiameter(tree_depth);

  // iterate over all children
  for (unsigned char child_idx = 0; child_idx < 8; child_idx++) {
    if (!this->branchHasChild(*node, child_idx))
      continue;

    const OctreeNode* child_node;
    child_node = this->getBranchChildPtr(*node, child_idx);

    OctreeKey new_key;
    PointT voxel_center;

    // generate new key for current branch voxel
    new_key.x = (key.x << 1) + (!!(child_idx & (1 << 2)));
    new_key.y = (key.y << 1) + (!!(child_idx & (1 << 1)));
    new_key.z = (key.z << 1) + (!!(child_idx & (1 << 0)));

    // generate voxel center point for voxel at key
    this->genVoxelCenterFromOctreeKey(new_key, tree_depth, voxel_center);

    // calculate distance to search point
    float squared_dist = this->pointSquaredDist(static_cast<const PointT&>(voxel_center), point);

    // if distance is smaller than search radius
    if (squared_dist + this->epsilon_ <=
        voxel_squared_diameter / 4.0 + radiusSquared +
            sqrt(voxel_squared_diameter * radiusSquared)) {

      if (child_node->getNodeType() == BRANCH_NODE) {
        // we have not reached maximum tree depth
        getNeighborsWithinRadiusRecursive(point,
                                          radiusSquared,
                                          static_cast<const BranchNode*>(child_node),
                                          new_key,
                                          tree_depth + 1,
                                          leafs,
                                          k_sqr_distances,
                                          max_nn);
        if (max_nn != 0 && leafs.size() == max_nn)
          return;
      }
      else {
        // we reached leaf node level
        const LeafNode* child_leaf = static_cast<const LeafNode*>(child_node);
        //Indices decoded_point_vector;
        LeafContainerT leafContainer = child_leaf->getContainer();
        leafs.push_back(leafContainer);
        k_sqr_distances.push_back(squared_dist);
      }
    }
  }
}


#define PCL_INSTANTIATE_OctreeSegmentCovariance(T) template class PCL_EXPORTS pcl::octree::OctreeSegmentCovariance<T>;

#endif /* SOURCE_DIRECTORY__POINTCLOUD_COMBINER_SRC_OCTREE_SEGMENT_COVARIANCE_HPP_ */
