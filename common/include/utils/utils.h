/*
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef POINTCLOUD_COMBINER_SRC_UTILS_H_
#define POINTCLOUD_COMBINER_SRC_UTILS_H_

#include <glob.h> // glob(), globfree()
#include "boost/filesystem.hpp"
#include <tf/transform_datatypes.h>
#include <Eigen/Dense>
#include <time.h>
#include <ros/time.h>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <numeric>      // std::iota
#include <algorithm>

namespace Utils {

inline void glob( const std::string& pattern, std::vector<std::string>& filenames ) {
  using namespace std;

  // glob struct resides on the stack
  glob_t glob_result;
  memset(&glob_result, 0, sizeof(glob_result));

  // do the glob operation
  int return_value = glob(pattern.c_str(), GLOB_TILDE, NULL, &glob_result);
  if(return_value != 0) {
    globfree(&glob_result);
    stringstream ss;
    ss << "glob() failed with return_value " << return_value << endl;
    throw std::runtime_error(ss.str());
  }

  // collect all the filenames into a std::vector<std::string>
  for(size_t i = 0; i < glob_result.gl_pathc; ++i) {
    filenames.push_back(string(glob_result.gl_pathv[i]));
  }

  std::sort( filenames.begin(), filenames.end() );

  // cleanup
  globfree(&glob_result);
}


// Return true if directory was created or it exists and false if failed
inline bool mkdir(std::string path) {
  if ( !boost::filesystem::exists(path.c_str()) ) {
    if ( !boost::filesystem::create_directories(path.c_str()) ) {
      ROS_ERROR_STREAM("Failed to create directory " << path);
      return false;
    }
    return true;
  }
  else
    return true;
}


} // Utils namespace

#endif /* POINTCLOUD_COMBINER_SRC_UTILS_H_ */
