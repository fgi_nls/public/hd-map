/*
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INSTANCE_SEGMENTATION_SRC_LABELDEFS_H_
#define INSTANCE_SEGMENTATION_SRC_LABELDEFS_H_

#include <map>

#define numLabels 19

enum Label {
  unlabeled     =  0,
  car           = 10,
  bicycle       = 11,
  motorcycle    = 15,
  truck         = 18,
  other_vehicle = 20,
  person        = 30,
  bicyclist     = 31,
  motorcyclist  = 32,
  road          = 40,
  parking       = 44,
  sidewalk      = 48,
  other_ground  = 49,
  building      = 50,
  fence         = 51,
  vegetation    = 70,
  trunk         = 71,
  terrain       = 72,
  pole          = 80,
  traffic_sign  = 81,
  ground        = 100
};


const std::map<Label, std::string> label2String = {{car,           "car"},
                                                   {bicycle,       "bicycle"},
                                                   {motorcycle,    "motorcycle"},
                                                   {truck,         "truck"},
                                                   {other_vehicle, "other_vehicle"},
                                                   {person,        "person"},
                                                   {bicyclist,     "bicyclist"},
                                                   {motorcyclist,  "motorcyclist"},
                                                   {road,          "road"},
                                                   {parking,       "parking"},
                                                   {sidewalk,      "sidewalk"},
                                                   {other_ground,  "other_ground"},
                                                   {building,      "building"},
                                                   {fence,         "fence"},
                                                   {vegetation,    "vegetation"},
                                                   {trunk,         "tree_trunk"},
                                                   {terrain,       "terrain"},
                                                   {pole,          "pole"},
                                                   {traffic_sign,  "traffic_sign"},
                                                   {ground,        "ground"}};


const std::map<Label, uint16_t> label2IdxMap = {{car,           0},
                                                {bicycle,       1},
                                                {motorcycle,    2},
                                                {truck,         3},
                                                {other_vehicle, 4},
                                                {person,        5},
                                                {bicyclist,     6},
                                                {motorcyclist,  7},
                                                {road,          8},
                                                {parking,       9},
                                                {sidewalk,     10},
                                                {other_ground, 11},
                                                {building,     12},
                                                {fence,        13},
                                                {vegetation,   14},
                                                {trunk,        15},
                                                {terrain,      16},
                                                {pole,         17},
                                                {traffic_sign, 18}};

const std::map<uint16_t, Label> idx2LabelMap = {{0,  car          },
                                                {1,  bicycle      },
                                                {2,  motorcycle   },
                                                {3,  truck        },
                                                {4,  other_vehicle},
                                                {5,  person       },
                                                {6,  bicyclist    },
                                                {7,  motorcyclist },
                                                {8,  road         },
                                                {9,  parking      },
                                                {10, sidewalk     },
                                                {11, other_ground },
                                                {12, building     },
                                                {13, fence        },
                                                {14, vegetation   },
                                                {15, trunk        },
                                                {16, terrain      },
                                                {17, pole         },
                                                {18, traffic_sign }};

const std::map<uint16_t, std::string> idx2LabelStringMap = {{0,  "car"          },
                                                            {1,  "bicycle"      },
                                                            {2,  "motorcycle"   },
                                                            {3,  "truck"        },
                                                            {4,  "other_vehicle"},
                                                            {5,  "person"       },
                                                            {6,  "bicyclist"    },
                                                            {7,  "motorcyclist" },
                                                            {8,  "road"         },
                                                            {9,  "parking"      },
                                                            {10, "sidewalk"     },
                                                            {11, "other_ground" },
                                                            {12, "building"     },
                                                            {13, "fence"        },
                                                            {14, "vegetation"   },
                                                            {15, "trunk"        },
                                                            {16, "terrain"      },
                                                            {17, "pole"         },
                                                            {18, "traffic_sign" }};


// https://www.codespeedy.com/hsv-to-rgb-in-cpp/
inline bool HSV2RGB(float H, float S, float V, int &R, int &G, int &B){
    if(H > 360.0f || H < 0.0f || S > 100.0f || S < 0.0f || V > 100.0f || V < 0.0f ){
        std::cerr << "The given HSV values are not in valid range (HSV): " << H << " " << S << " " << V << std::endl;
        return false;
    }
    float s = S / 100.0f;
    float v = V / 100.0f;
    float C = s * v;
    float X = C * (1.0f - fabs(fmod(H/60.0f, 2) - 1.0f));
    float m = v - C;
    float r, g, b;
    if(H >= 0.0f && H < 60.0f){
        r = C, g = X, b = 0.0f;
    }
    else if(H >= 60.0f && H < 120.0f){
        r = X, g = C, b = 0.0f;
    }
    else if(H >= 120.0f && H < 180.0f){
        r = 0.0f, g = C, b = X;
    }
    else if(H >= 180.0f && H < 240.0f){
        r = 0.0f, g = X, b = C;
    }
    else if(H >= 240.0f && H < 300.0f){
        r = X, g = 0.0f, b = C;
    }
    else{
        r = C, g = 0, b = X;
    }
    R = round((r + m) * 255.0f);
    G = round((g + m) * 255.0f);
    B = round((b + m) * 255.0f);

    return true;
}


inline bool getColorHSV(Label label, int &R, int &G, int &B, float probability) {
  float H, S = 100.0f, V = probability * 100.0f;
  bool error = false;
  switch(label) {
    case car:           H = 210.0f;            break;
    case bicycle:       H = 150.0f;            break;
    case motorcycle:    H = 180.0f;            break;
    case truck:         H = 210.0f; S = 25.0f; break;
    case other_vehicle: H = 240.0f;            break;
    case person:        H =   0.0f; S = 25.0f; break;
    case bicyclist:     H = 150.0f; S = 25.0f; break;
    case motorcyclist:  H = 180.0f; S = 25.0f; break;
    case road:          H = 300.0f;            break;
    case parking:       H = 270.0f; S = 25.0f; break;
    case sidewalk:      H = 270.0f;            break;
    case other_ground:  H = 330.0f;            break;
    case building:      H =  60.0f;            break;
    case fence:         H =  30.0f; S = 50.0f; break;
    case vegetation:    H = 120.0f;            break;
    case trunk:         H =  30.0f;            break;
    case terrain:       H =  90.0f; S = 50.0f; break;
    case pole:          H =  60.0f; S = 25.0f; break;
    case traffic_sign:  H =   0.0f;            break;
    case unlabeled:     H =   0.0f; S =  0.0f; break;
    default:            H =   0.0f; S =  0.0f; V = 0.0f; error = true; break;
  }

  HSV2RGB(H, S, V, R, G, B);

  if (error)
    std::cerr << H << " " << S << " " << V << " " << R << " " << G << " " << B << std::endl;

  return error;
}


inline bool getColorRGB(Label label, int &R, int &G, int &B) {
  bool error = false;
  switch(label) {
    case unlabeled:     R =   0; G =   0; B =   0; break;
    case car:           R = 100; G = 150; B = 245; break;
    case bicycle:       R = 100; G = 230; B = 245; break;
    case motorcycle:    R =  30; G =  60; B = 150; break;
    case truck:         R =  80; G =  30; B = 180; break;
    case other_vehicle: R =   0; G =   0; B = 255; break;
    case person:        R = 255; G =  30; B =  30; break;
    case bicyclist:     R = 255; G =  40; B = 200; break;
    case motorcyclist:  R = 150; G =  30; B =  90; break;
    case road:          R = 255; G =   0; B = 255; break;
    case parking:       R = 255; G = 150; B = 255; break;
    case sidewalk:      R =  75; G =   0; B =  75; break;
    case other_ground:  R = 175; G =   0; B =  75; break;
    case building:      R = 255; G = 200; B =   0; break;
    case fence:         R = 255; G = 120; B =  50; break;
    case vegetation:    R =   0; G = 175; B =   0; break;
    case trunk:         R = 135; G =  60; B =   0; break;
    case terrain:       R = 150; G = 240; B =  80; break;
    case pole:          R = 255; G = 240; B = 150; break;
    case traffic_sign:  R = 255; G =   0; B =   0; break;
    default:            R =   0; G =   0; B =   0; error = true; break;
  }

  return error;
}


inline bool getColor(Label label, int &R, int &G, int &B, float probability = -1.0f) {
  if (probability < 0.0f)
    return getColorRGB(label, R, G, B);
  else
    return getColorHSV(label, R, G, B, probability);
}


#endif /* INSTANCE_SEGMENTATION_SRC_LABELDEFS_H_ */
