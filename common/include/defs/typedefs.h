/*
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef INSTANCE_SEGMENTATION_SRC_TYPEDEFS_H_
#define INSTANCE_SEGMENTATION_SRC_TYPEDEFS_H_

#include <algorithm>
#include <vector>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PointIndices.h>
#include <pcl/point_representation.h>
#include <pcl/octree/octree_pointcloud_voxelcentroid.h>

#include "labeldefs.h"

// Convenient typedefs
typedef pcl::PointXYZ Point;
typedef pcl::PointCloud<Point> Cloud;
typedef pcl::PointCloud<Point>::Ptr CloudPtr;

typedef pcl::PointXYZI PointI;
typedef pcl::PointCloud<PointI> CloudI;
typedef pcl::PointCloud<PointI>::Ptr CloudIPtr;

typedef pcl::PointXYZL PointL;
typedef pcl::PointCloud<PointL> CloudL;
typedef pcl::PointCloud<PointL>::Ptr CloudLPtr;

typedef pcl::PointXYZRGB PointRGB;
typedef pcl::PointCloud<PointRGB> CloudRGB;
typedef pcl::PointCloud<PointRGB>::Ptr CloudRGBPtr;

typedef pcl::PointXYZRGBA PointRGBA;
typedef pcl::PointCloud<PointRGBA> CloudRGBA;
typedef pcl::PointCloud<PointRGBA>::Ptr CloudRGBAPtr;

typedef pcl::PointXYZRGBL PointRGBL;
typedef pcl::PointCloud<PointRGBL> CloudRGBL;
typedef pcl::PointCloud<PointRGBL>::Ptr CloudRGBLPtr;

/** Euclidean coordinate, including intensity, ring number and time. */
struct EIGEN_ALIGN16 PointXYZIRT
{
  PCL_ADD_POINT4D;                    // quad-word XYZ
  float         intensity;            // laser intensity reading
  std::uint16_t ring;                 // laser ring number
  double        time;								  // TIME
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW     // ensure proper alignment

  inline PointXYZIRT () {
    x = y = z = intensity = time = 0.0f;
    data[3] = 1.0f;
    ring = 0;
  }

  inline bool ringBelongs(const std::vector<uint16_t> &rings, bool inverse = false) const {
    if (inverse)
      return !std::binary_search(rings.begin(), rings.end(), ring);

    return std::binary_search(rings.begin(), rings.end(), ring);
  }
};

/** Euclidean coordinate, including intensity, ring number, time and squared distance of point. */
struct EIGEN_ALIGN16 PointXYZIRTD : public PointXYZIRT
{
  float 	 sq_dist;									  // scanned point squared distance
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW     // ensure proper alignment

  inline PointXYZIRTD () {
    x = y = z = intensity = time = sq_dist = 0.0f;
    data[3] = 1.0f;
    ring = 0;
  }

  inline bool operator<(const double &sq_range) const {
    return sq_dist < sq_range;
  }
};

/** Euclidean coordinate, including intensity, ring number, time, label and squared distance of point. */
struct EIGEN_ALIGN16 PointXYZIRTLD : public PointXYZIRTD
{
  uint16_t inst_label;                // point label
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW     // ensure proper alignment

  inline PointXYZIRTLD () {
    x = y = z = intensity = time = sq_dist = 0.0f;
    data[3] = 1.0f;
    ring = inst_label = 0;
  }

  inline uint16_t getInstLabel() const {
    return inst_label;
  }
};

/** Euclidean coordinate, including intensity, ring number, time, squared distance and probability of labels. */
struct EIGEN_ALIGN16 PointXYZIRTDLabelProb : public PointXYZIRTD
{
  float    sem_labels[numLabels];      // label probabilities
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW      // ensure proper alignment

  inline PointXYZIRTDLabelProb () {
    x = y = z = intensity = time = sq_dist = 0.0f;
    data[3] = 1.0f;
    ring = 0;
    for (int i = 0; i < numLabels; i++)
      sem_labels[i] = 0.0f;
  }

  inline uint16_t getHighestSemLabelIdx() const {
    int N = sizeof(sem_labels) / sizeof(sem_labels[0]);
    return std::distance(sem_labels, std::max_element(sem_labels, sem_labels + N));
  }

  inline uint16_t getSemLabel() const {
    return idx2LabelMap.at(getHighestSemLabelIdx());
  }

  inline bool labelBelongs(const int &labelNot, const int &labelGreater) const {
    int index = getHighestSemLabelIdx();

    return (index != labelNot && index > labelGreater);
  }

  inline void toRGBIntensity(PointRGB &point) {
    int label_idx = getHighestSemLabelIdx();
    float highest_prob = sem_labels[label_idx];
    int R, G, B;
    try {
      if (getColor (idx2LabelMap.at(label_idx), R, G, B, highest_prob) )
        std::cerr << "Got undefined color code, cannot interpret! Index: " << label_idx << " Label: " << idx2LabelMap.at(label_idx) << std::endl;
      else {
        point.x = this->x;
        point.y = this->y;
        point.z = this->z;
        point.r = R;
        point.g = G;
        point.b = B;
      }

    }
    catch (const std::out_of_range& oor) {
      std::cerr << "Key " << label_idx << " not found in map: " << oor.what() << std::endl;
    }

    return;
  }

  inline void toRGB(PointRGB &point) {
    int label_idx = getHighestSemLabelIdx();
    int R, G, B;
    try {
      if (getColorRGB (idx2LabelMap.at(label_idx), R, G, B) )
        std::cerr << "Got undefined color code, cannot interpret! Index: " << label_idx << " Label: " << idx2LabelMap.at(label_idx) << std::endl;
      else {
        point.x = this->x;
        point.y = this->y;
        point.z = this->z;
        point.r = R;
        point.g = G;
        point.b = B;
      }

    }
    catch (const std::out_of_range& oor) {
      std::cerr << "Key " << label_idx << " not found in map: " << oor.what() << std::endl;
    }

    return;
  }
};


/** Euclidean coordinate including intensity,
 *                                 ring number,
 *                                 time,
 *                                 measured distance
 *                                 semantic label probability
 *                                 instance label
 **/
struct EIGEN_ALIGN16 PointXYZIRTD2L : public PointXYZIRTLD
{
  float         sem_labels[numLabels]; // semantic label probability vector
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW      // ensure proper alignment

  inline PointXYZIRTD2L () {
    x = y = z = intensity = time = sq_dist = 0.0f;
    data[3] = 1.0f;
    ring = inst_label = 0;
    for (int i = 0; i < numLabels; i++)
      sem_labels[i] = 0.0f;
  }

  inline std::uint16_t getHighestSemLabelIdx() const {
    int N = sizeof(sem_labels) / sizeof(sem_labels[0]);
    return std::distance(sem_labels, std::max_element(sem_labels, sem_labels + N));
  }

  inline std::uint16_t getSemLabel() const {
    return idx2LabelMap.at(getHighestSemLabelIdx());
  }

  inline bool labelBelongs(const int &labelNot, const int &labelGreater) const {
    int index = getHighestSemLabelIdx();

    return (index != labelNot &&
            index > labelGreater);
  }

  inline void toRGBIntensity(PointRGB &point) {
    int label_idx = getHighestSemLabelIdx();
    float highest_prob = sem_labels[label_idx];
    int R, G, B;
    try {
      if (getColor (idx2LabelMap.at(label_idx), R, G, B, highest_prob) )
        std::cerr << "Got undefined color code, cannot interpret! Index: " << label_idx << " Label: " << idx2LabelMap.at(label_idx) << std::endl;
      else {
        point.x = this->x;
        point.y = this->y;
        point.z = this->z;
        point.r = R;
        point.g = G;
        point.b = B;
      }

    }
    catch (const std::out_of_range& oor) {
      std::cerr << "Key " << label_idx << " not found in map: " << oor.what() << std::endl;
    }

    return;
  }

  inline void toRGB(PointRGB &point) {
    int label_idx = getHighestSemLabelIdx();
    int R, G, B;
    try {
      if (getColor (idx2LabelMap.at(label_idx), R, G, B) )
        std::cerr << "Got undefined color code, cannot interpret! Index: " << label_idx << " Label: " << idx2LabelMap.at(label_idx) << std::endl;
      else {
        point.x = this->x;
        point.y = this->y;
        point.z = this->z;
        point.r = R;
        point.g = G;
        point.b = B;
      }

    }
    catch (const std::out_of_range& oor) {
      std::cerr << "Key " << label_idx << " not found in map: " << oor.what() << std::endl;
    }

    return;
  }
};

POINT_CLOUD_REGISTER_POINT_STRUCT(PointXYZIRT,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, intensity, intensity)
                                  (std::uint16_t, ring, ring)
				  												(double, time, time))

POINT_CLOUD_REGISTER_POINT_STRUCT(PointXYZIRTD,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, intensity, intensity)
                                  (std::uint16_t, ring, ring)
				  												(double, time, time)
				  												(float, sq_dist, sq_dist))
			  												
POINT_CLOUD_REGISTER_POINT_STRUCT(PointXYZIRTLD,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, intensity, intensity)
                                  (std::uint16_t, ring, ring)
				  												(double, time, time)
				  												(std::uint16_t, inst_label, inst_label)
				  												(float, sq_dist, sq_dist))
				  												
POINT_CLOUD_REGISTER_POINT_STRUCT(PointXYZIRTDLabelProb,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, intensity, intensity)
                                  (std::uint16_t, ring, ring)
                                  (double, time, time)
                                  (float, sq_dist, sq_dist)
                                  (float[numLabels], sem_labels, sem_labels))

POINT_CLOUD_REGISTER_POINT_STRUCT(PointXYZIRTD2L,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z)
                                  (float, intensity, intensity)
                                  (std::uint16_t, ring, ring)
                                  (double, time, time)
                                  (float, sq_dist, sq_dist)
                                  (float[numLabels], sem_labels, sem_labels)
                                  (std::uint16_t, inst_label, inst_label))

typedef PointXYZIRT PointT;
typedef pcl::PointCloud<PointT> CloudT;
typedef pcl::PointCloud<PointT>::Ptr CloudTPtr;
typedef std::vector<PointT, Eigen::aligned_allocator<PointT> >::iterator PointIter;

typedef PointXYZIRTD PointTD;
typedef pcl::PointCloud<PointTD> CloudTD;
typedef pcl::PointCloud<PointTD>::Ptr CloudTDPtr;

typedef PointXYZIRTLD PointTLD;
typedef pcl::PointCloud<PointTLD> CloudTLD;
typedef pcl::PointCloud<PointTLD>::Ptr CloudTLDPtr;

typedef PointXYZIRTDLabelProb PointTDLabelProb;
typedef pcl::PointCloud<PointTDLabelProb> CloudTDLabelProb;
typedef pcl::PointCloud<PointTDLabelProb>::Ptr CloudTDLabelProbPtr;
typedef pcl::PointCloud<PointTDLabelProb>::ConstPtr CloudTDLabelProbConstPtr;

typedef PointXYZIRTD2L PointTD2L;
typedef pcl::PointCloud<PointTD2L> CloudTD2L;
typedef pcl::PointCloud<PointTD2L>::Ptr CloudTD2LPtr;

typedef pcl::PointIndices Indices;
typedef pcl::PointIndicesPtr IndicesPtr;
typedef pcl::PointIndicesConstPtr IndicesConstPtr;

typedef pcl::octree::OctreePointCloudVoxelCentroid<PointTLD> OctreeTLD;
typedef pcl::octree::OctreePointCloudVoxelCentroid<PointTLD>::Ptr OctreeTLDPtr;
typedef pcl::octree::OctreePointCloud<PointTLD>::IndicesPtr OctreeTLDIndicesPtr;

typedef pcl::octree::OctreePointCloudVoxelCentroid<PointTDLabelProb> OctreeTDLabelProb;
typedef pcl::octree::OctreePointCloudVoxelCentroid<PointTDLabelProb>::Ptr OctreeTDLabelProbPtr;
typedef pcl::octree::OctreePointCloud<PointTDLabelProb>::IndicesPtr OctreeTDLabelProbIndicesPtr;

typedef pcl::octree::OctreePointCloudVoxelCentroid<PointRGBL> OctreeRGBL;
typedef pcl::octree::OctreePointCloudVoxelCentroid<PointRGBL>::Ptr OctreeRGBLPtr;
typedef pcl::octree::OctreePointCloud<PointRGBL>::IndicesPtr OctreeRGBLIndicesPtr;


template <typename PointT>
typename std::enable_if<pcl::traits::has_field<PointT, pcl::fields::sem_labels>::value>::type
makeRGBIntensityCloud(pcl::PointCloud<PointT> &cloud, CloudRGB &cloudRGB) {
  cloudRGB.resize(cloud.size());
  for (int i = 0; i < cloud.size(); i++) {
    cloud.points[i].toRGBIntensity(cloudRGB.points[i]);
  }
}

template <typename PointT>
typename std::enable_if<!pcl::traits::has_field<PointT, pcl::fields::sem_labels>::value>::type
makeRGBIntensityCloud(pcl::PointCloud<PointT> &cloud, CloudRGB &cloudRGB) {

}

template <typename PointT>
typename std::enable_if<pcl::traits::has_field<PointT, pcl::fields::sem_labels>::value>::type
makeRGBCloud(pcl::PointCloud<PointT> &cloud, CloudRGB &cloudRGB) {
  cloudRGB.resize(cloud.size());
  for (int i = 0; i < cloud.size(); i++) {
    cloud.points[i].toRGB(cloudRGB.points[i]);
  }
}

template <typename PointT>
typename std::enable_if<!pcl::traits::has_field<PointT, pcl::fields::sem_labels>::value>::type
makeRGBCloud(pcl::PointCloud<PointT> &cloud, CloudRGB &cloudRGB) {

}

#endif /* INSTANCE_SEGMENTATION_SRC_TYPEDEFS_H_ */
