/*
 *  Software License Agreement (BSD License)
 *
 *  Copyright (c) 2021-2022, Petri Manninen,
 *                           Department of Remote Sensing and Photogrammetry,
 *                           Finnish Geospatial Research Institute (FGI),
 *                           National Land Survey of Finland (NLS).
 *
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the copyright holder(s) nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef POINTCLOUD_COMBINER_SRC_INSTANCESEGMENTER_H_
#define POINTCLOUD_COMBINER_SRC_INSTANCESEGMENTER_H_

#include <map>

#include <pcl/segmentation/conditional_euclidean_clustering.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/search/kdtree.h>
#include <pcl/common/centroid.h>
#include <pcl/console/time.h>

#include <ros/ros.h>

#include "defs/typedefs.h"
#include "defs/labeldefs.h"

template <typename PointT>
class InstanceClustering : public pcl::PCLBase<PointT> {
  using PCLBase = pcl::PCLBase<PointT>;

public:
  typedef typename pcl::PointCloud<PointT> CloudT;
  typedef typename pcl::PointCloud<PointT>::Ptr CloudTPtr;
  typedef typename pcl::PointCloud<PointT>::ConstPtr CloudTConstPtr;

  typedef typename pcl::PointCloud<pcl::PointXYZ> CloudXYZ;
  typedef typename pcl::PointCloud<pcl::PointXYZ>::Ptr CloudXYZPtr;

  InstanceClustering();
  virtual ~InstanceClustering();

  void initSegments ();
  void findInstanceClusters(std::vector<Label> &semanticLabels = std::vector<Label>());
  void defineInstanceDistributions();

  void setClusterTolerance(float tolerance);
  void setMinClusterSize(int number);

  void visualizeInstances (CloudRGBLPtr cloud);
  void visualizeInstances (CloudRGBLPtr cloud, CloudRGBLPtr CloudCentroids);
  void getCentroidCloud (CloudLPtr cloud);
  void getCloud(const Label &label, CloudTPtr &cloud, pcl::IndicesClustersPtr &instanceClusters);

  std::map <uint16_t, pcl::PointIndicesPtr> semanticSegments_;
  std::map <uint16_t, std::vector<pcl::PointIndices> > instanceSegments_;

private:
  using PCLBase::input_;
  using PCLBase::indices_;
  using PCLBase::initCompute;

  void euclideanClusters (CloudXYZPtr cloud, std::vector<pcl::PointIndices> &cluster_indices, pcl::PointIndicesPtr useIndices = nullptr );

  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree_;
  CloudXYZPtr cloudXYZ_;

  float clusterTolerance_;
  int minClusterSize_;

  std::vector<Label> groundLabels_ = {road, sidewalk, parking, other_ground, terrain};

};


template <typename PointT>
InstanceClustering<PointT>::InstanceClustering() :
  tree_ (new pcl::search::KdTree<pcl::PointXYZ>),
  cloudXYZ_ (new CloudXYZ),
  clusterTolerance_ (0.3),
  minClusterSize_ (50)
{
  groundLabels_ = {road, sidewalk, parking, other_ground, terrain};
}


template <typename PointT>
InstanceClustering<PointT>::~InstanceClustering() {

}


template <typename PointT>
void InstanceClustering<PointT>::setClusterTolerance(float tolerance) {
  clusterTolerance_ = tolerance;
}


template <typename PointT>
void InstanceClustering<PointT>::setMinClusterSize(int number) {
  minClusterSize_ = number;
}


template <typename PointT>
void InstanceClustering<PointT>::initSegments () {
  semanticSegments_.clear();
  instanceSegments_.clear();
  cloudXYZ_->clear();
  cloudXYZ_->resize(input_->size());
  for (int i = 0; i < input_->size(); i++) {
    auto &p = input_->points[i];
    if (std::isnan(p.x) || std::isinf(p.x) || std::isnan(p.y) || std::isinf(p.y) || std::isnan(p.z) || std::isinf(p.z) ) {
      ROS_ERROR_STREAM("Point has got nan/inf values: " << p.x << " " << p.y << " " << p.z);
      continue;
    }

    cloudXYZ_->points[i].x = p.x;
    cloudXYZ_->points[i].y = p.y;
    cloudXYZ_->points[i].z = p.z;

    uint16_t label = input_->at(i).getSemLabel();
    if (std::find(groundLabels_.begin(), groundLabels_.end(), label) != groundLabels_.end() )
      label = ground;
    if (!semanticSegments_.count(label))
      semanticSegments_[label] = pcl::PointIndicesPtr (new pcl::PointIndices);
    semanticSegments_[label]->indices.push_back(i);
  }
  tree_->setInputCloud (cloudXYZ_);
}


template <typename PointT>
void InstanceClustering<PointT>::findInstanceClusters (std::vector<Label> &semanticLabels) {
  for (auto semanticLabel : semanticLabels) {
    pcl::console::TicToc tt;
    tt.tic ();
    std::string print = "    Clustering instances from " + label2String.at(semanticLabel) + " semantic segment...";
    std::cout << std::setw(63) << std::left << print << std::flush;
    euclideanClusters(cloudXYZ_, instanceSegments_[semanticLabel], semanticSegments_[semanticLabel]);
    std::cout << ">> Done: " << std::setw(8) << std::right << std::fixed << std::setprecision(3) << tt.toc () / 1e3
              << " sec, " << std::setw(4) << instanceSegments_[semanticLabel].size() << " instances found.\n";
  }
}


template <typename PointT>
void InstanceClustering<PointT>::euclideanClusters (CloudXYZPtr cloud, std::vector<pcl::PointIndices> &cluster_indices, pcl::PointIndicesPtr useIndices ) {

  pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
  ec.setClusterTolerance ( clusterTolerance_ );
  ec.setMinClusterSize ( minClusterSize_ );
  ec.setInputCloud (cloud);

  if ( useIndices != nullptr )
    ec.setIndices (useIndices);

  ec.setSearchMethod (tree_);
  ec.extract (cluster_indices);

  return;
}


template <typename PointT>
void InstanceClustering<PointT>::getCloud(const Label &label, CloudTPtr &cloud, pcl::IndicesClustersPtr &instanceClusters) {
  cloud->reserve(cloud->size() + semanticSegments_[label]->indices.size());

  if (label == ground) {
    instanceClusters->resize(1);
    for (auto &idx : semanticSegments_[label]->indices) {
      PointT point;
      pcl::copyPoint(input_->at(idx), point);
      instanceClusters->at(0).indices.push_back(cloud->size());
      cloud->points.push_back(point);
    }
    return;
  }

  instanceClusters->resize(instanceSegments_[label].size());

  uint16_t instLabelNum = 0;
  for (auto &instance : instanceSegments_[label]) {
    for (auto &idx : instance.indices) {
      PointT point;
      pcl::copyPoint(input_->at(idx), point);
      point.inst_label = instLabelNum;
      instanceClusters->at(instLabelNum).indices.push_back(cloud->size());
      cloud->points.push_back(point);

    }
    instLabelNum++;
  }
}

#endif /* POINTCLOUD_COMBINER_SRC_INSTANCESEGMENTER_H_ */
